﻿using DesafioConfitec.Tests.Units.Core.Fixtures;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace DesafioConfitec.Tests.Units.Domain.Entities.Fixtures
{
    [ExcludeFromCodeCoverage]
    [CollectionDefinition(nameof(BaseBogusUnitDomainEntityCollection))]
    public class BaseBogusUnitDomainEntityCollection : ICollectionFixture<UsuarioTestDomainEntityFixture>
    {
    }
}