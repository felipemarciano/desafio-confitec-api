﻿using DesafioConfitec.Dominio.Entities;
using DesafioConfitec.Dominio.Notify;
using DesafioConfitec.Dominio.Validators;
using DesafioConfitec.Tests.Units.Core.Fixtures;
using DesafioConfitec.Tests.Units.Domain.Entities.Fixtures;
using FluentAssertions;
using Xunit;
using Xunit.Abstractions;

namespace DesafioConfitec.Tests.Units.Domain.Entities.Usuarios
{
    [Collection(nameof(BaseBogusUnitDomainEntityCollection))]
    public class NovoUsuarioTest
    {
        #region Private Fields

        private readonly ITestOutputHelper _testOutputHelper;

        private readonly UsuarioTestDomainEntityFixture _usuarioTesteFixture;

        #endregion Private Fields

        #region Public Constructors

        public NovoUsuarioTest(UsuarioTestDomainEntityFixture usuarioTesteFixture, ITestOutputHelper testOutputHelper)
        {
            _usuarioTesteFixture = usuarioTesteFixture;
            _testOutputHelper = testOutputHelper;
        }

        #endregion Public Constructors

        #region Public Methods

        [Trait(nameof(Usuario), "Teste de unidade de entidade")]
        [Fact(DisplayName = "Novo usuário inválido")]
        public void Novo_Usuario_Invalido()
        {
            var usuario = _usuarioTesteFixture.GerarNovoUsuarioInvalido();

            var usuarioValidator = new UsuarioValidator(usuario, true, "123");

            usuarioValidator.ValidationResult.Errors.Should().Contain(usuarioValidator.ValidationResult.Errors,
                                              UsuarioValidator.MensagemNomeMaximoCaracteres,
                                              UsuarioValidator.MensagemSobrenomeMaximoCaracteres,
                                              UsuarioValidator.MensagemSenhaMaximoCaracteres,
                                              UsuarioValidator.MensagemSenhasNaoConferem,
                                              UsuarioValidator.MensagemDataNascimentoForaDoRange,
                                              UsuarioValidator.MensagemEmailInvalido,
                                              UsuarioValidator.MensagemEmailMaximoCaractereres,
                                              UsuarioValidator.MensagemEscolaridadeInvalida);

            _testOutputHelper.WriteLine($"Erros encontrados: {string.Join(", ", usuarioValidator.ValidationResult.Errors)}");
        }

        [Trait(nameof(Usuario), "Teste de unidade de entidade")]
        [Fact(DisplayName = "Novo usuário válido")]
        public void Novo_Usuario_Valido()
        {
            var notification = new Notification<object>();

            var usuario = _usuarioTesteFixture.GerarNovoUsuarioValido();

            var usuarioValidator = new UsuarioValidator(usuario, true, usuario.Senha);
            notification.AddError(usuarioValidator.ValidationResult);

            notification.Success.Should().BeTrue();
        }

        #endregion Public Methods
    }
}