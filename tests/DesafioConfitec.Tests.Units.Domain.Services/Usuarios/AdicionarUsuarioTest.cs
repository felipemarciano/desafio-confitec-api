﻿using DesafioConfitec.Dominio.AutoMapper;
using DesafioConfitec.Dominio.Entities;
using DesafioConfitec.Dominio.Validators;
using DesafioConfitec.Tests.Units.Core.Fixtures;
using DesafioConfitec.Tests.Units.Domain.Services.Fixtures;
using FluentAssertions;
using Moq;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace DesafioConfitec.Tests.Units.Domain.Services.Usuarios
{
    [Collection(nameof(BaseBogusUnitServiceCollection))]
    public class AdicionarUsuarioTest
    {
        #region Private Fields

        private readonly ITestOutputHelper _testOutputHelper;

        private readonly UsuarioTestDomainEntityFixture _usuarioTesteFixture;

        #endregion Private Fields

        #region Public Constructors

        public AdicionarUsuarioTest(UsuarioTestDomainEntityFixture usuarioTesteFixture, ITestOutputHelper testOutputHelper)
        {
            _usuarioTesteFixture = usuarioTesteFixture;
            _testOutputHelper = testOutputHelper;
        }

        #endregion Public Constructors

        #region Public Methods

        [Trait(nameof(Usuario), "Teste de unidade de serviço")]
        [Fact(DisplayName = "Adicionar usuário duplicado")]
        public async Task Adicionar_Usuario_DuplicadoAsync()
        {
            var usuarioDto = _usuarioTesteFixture.CadastrarUsuarioValido();
            var usuario = usuarioDto.ToEntity();

            _usuarioTesteFixture.Repository.Setup(s => s.VerificarDuplicidadeAsync(0, usuarioDto.Nome, usuarioDto.Sobrenome, usuarioDto.Email)).ReturnsAsync(true);
            _usuarioTesteFixture.Repository.Setup(s => s.Adicionar(usuario));

            var result = await _usuarioTesteFixture.Service.AdicionarAsync(usuarioDto).ConfigureAwait(false);

            result.Success.Should().BeFalse();

            _testOutputHelper.WriteLine(result.ToString());
        }

        [Trait(nameof(Usuario), "Teste de unidade de serviço")]
        [Fact(DisplayName = "Adicionar usuário inválido")]
        public async Task Adicionar_Usuario_InvalidoAsync()
        {
            var usuarioDto = _usuarioTesteFixture.CadastrarUsuarioInvalido();
            var usuario = usuarioDto.ToEntity();

            _usuarioTesteFixture.Repository.Setup(s => s.VerificarDuplicidadeAsync(0, usuarioDto.Nome, usuarioDto.Sobrenome, usuarioDto.Email)).ReturnsAsync(false);
            _usuarioTesteFixture.Repository.Setup(s => s.Adicionar(usuario));

            var result = await _usuarioTesteFixture.Service.AdicionarAsync(usuarioDto).ConfigureAwait(false);

            result.Success.Should().BeFalse();

            result.Errors.Should().Contain(result.Errors,
                                            UsuarioValidator.MensagemSenhasNaoConferem,
                                            UsuarioValidator.MensagemDataNascimentoForaDoRange,
                                            UsuarioValidator.MensagemEmailInvalido,
                                            UsuarioValidator.MensagemEscolaridadeInvalida);

            _testOutputHelper.WriteLine($"Erros encontrados: {result.ToString()}");
        }

        [Trait(nameof(Usuario), "Teste de unidade de serviço")]
        [Fact(DisplayName = "Adicionar usuário válido")]
        public async Task Adicionar_Usuario_ValidoAsync()
        {
            var usuarioDto = _usuarioTesteFixture.CadastrarUsuarioValido();
            var usuario = usuarioDto.ToEntity();

            _usuarioTesteFixture.Repository.Setup(s => s.VerificarDuplicidadeAsync(0, usuarioDto.Nome, usuarioDto.Sobrenome, usuarioDto.Email)).ReturnsAsync(false);
            _usuarioTesteFixture.Repository.Setup(s => s.Adicionar(usuario));

            var result = await _usuarioTesteFixture.Service.AdicionarAsync(usuarioDto).ConfigureAwait(false);

            result.Success.Should().BeTrue();

            _usuarioTesteFixture.Repository.Object.Adicionar(usuario);
            _usuarioTesteFixture.Repository.Verify(v => v.Adicionar(usuario), Times.Once);

            _testOutputHelper.WriteLine(result.Message);
        }

        #endregion Public Methods
    }
}