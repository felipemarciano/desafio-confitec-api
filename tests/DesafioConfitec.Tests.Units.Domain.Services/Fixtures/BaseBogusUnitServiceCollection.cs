﻿using DesafioConfitec.Tests.Units.Core.Fixtures;
using System.Diagnostics.CodeAnalysis;
using Xunit;

namespace DesafioConfitec.Tests.Units.Domain.Services.Fixtures
{
    [ExcludeFromCodeCoverage]
    [CollectionDefinition(nameof(BaseBogusUnitServiceCollection))]
    public class BaseBogusUnitServiceCollection : ICollectionFixture<UsuarioTestDomainEntityFixture>
    {
    }
}