﻿using Bogus;
using DesafioConfitec.Dominio.AutoMapper;
using DesafioConfitec.Dominio.Dto;
using DesafioConfitec.Dominio.Entities;
using DesafioConfitec.Dominio.Enums;
using DesafioConfitec.Dominio.Interfaces.Repositories;
using DesafioConfitec.Dominio.Services;
using DesafioConfitec.Tests.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace DesafioConfitec.Tests.Units.Core.Fixtures
{
    [ExcludeFromCodeCoverage]
    public class UsuarioTestDomainEntityFixture : BaseTestFixture<UsuarioService, IUsuarioRepository>
    {
        #region Public Methods

        public UsuarioCadastroDto CadastrarUsuarioInvalido()
        {
            var usuario = new Faker<UsuarioCadastroDto>("pt_BR").CustomInstantiator(c =>
                new UsuarioCadastroDto
                {
                    Nome = c.Name.FullName(),
                    Sobrenome = c.Name.FullName(),
                    Email = c.Name.FullName(),
                    Senha = c.Internet.Password(10),
                    ConfirmacaoSenha = "",
                    DataNascimento = new DateTime(1900, 1, 1),
                    Escolaridade = 100
                }
            );

            return usuario;
        }

        public UsuarioCadastroDto CadastrarUsuarioValido()
        {
            var usuario = new Faker<UsuarioCadastroDto>("pt_BR").CustomInstantiator(c =>
                new UsuarioCadastroDto
                {
                    Nome = c.Name.FirstName(),
                    Sobrenome = c.Name.LastName(),
                    Email = c.Internet.Email(),
                    Senha = c.Internet.Password(10),
                    DataNascimento = c.Date.Between(DateTime.Now.AddYears(-70), DateTime.Now.AddYears(-20)),
                    Escolaridade = EnumEscolaridade.Medio.GetHashCode()
                }
            ).Generate(1).First();

            usuario.ConfirmacaoSenha = usuario.Senha;
            return usuario;
        }

        public IEnumerable<Usuario> GerarListaNovoUsuarioInvalidoValido()
        {
            var maxCaracteres = "";
            var nome = new Faker<string>("pt_BR")
                .CustomInstantiator(c =>
                                    maxCaracteres = c.Name.FullName() +
                                                    c.Name.FullName() +
                                                    c.Name.FullName() +
                                                    c.Name.FullName() +
                                                    c.Name.FullName() +
                                                    c.Name.FullName() +
                                                    c.Name.FullName());

            var usuarios = new Faker<Usuario>("pt_BR").CustomInstantiator(c =>
             new Usuario(
                maxCaracteres,
                maxCaracteres,
                maxCaracteres,
                c.Internet.Password(20),
                new DateTime(1900, 1, 1),
                100
             )).Generate(10);

            return usuarios;
        }

        public IEnumerable<Usuario> GerarListaNovoUsuarioValido()
        {
            var usuarios = new Faker<Usuario>("pt_BR")
             .CustomInstantiator(c =>
             new Usuario(
                 c.Name.FindName(),
                 c.Name.LastName(),
                 c.Internet.Email(),
                 c.Internet.Password(5),
                 c.Date.Between(DateTime.Now.AddYears(-70), DateTime.Now.AddYears(-20)),
                 EnumEscolaridade.Infantil.GetHashCode()
             )).Generate(10);

            return usuarios;
        }

        public UsuarioDto GerarNovoUsuarioDtoInvalido()
        {
            var usuario = GerarListaNovoUsuarioInvalidoValido().FirstOrDefault();
            return usuario.ToResult();
        }

        public UsuarioDto GerarNovoUsuarioDtoValido()
        {
            var usuario = GerarListaNovoUsuarioValido().FirstOrDefault();
            return usuario.ToResult();
        }

        public Usuario GerarNovoUsuarioInvalido()
        {
            var usuario = GerarListaNovoUsuarioInvalidoValido().FirstOrDefault();
            return usuario;
        }

        public Usuario GerarNovoUsuarioValido()
        {
            var usuario = GerarListaNovoUsuarioValido().FirstOrDefault();
            return usuario;
        }

        public Usuario RetornarUsuarioNulo()
        {
            return null;
        }

        #endregion Public Methods
    }
}