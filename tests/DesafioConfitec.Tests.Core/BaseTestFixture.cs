﻿using DesafioConfitec.Dominio.Services;
using Moq;
using Moq.AutoMock;
using System;

namespace DesafioConfitec.Tests.Core
{
    public class BaseTestFixture<TService, TRepository> : IDisposable
        where TService : BaseService
        where TRepository : class
    {
        #region Private Fields

        private bool _disposed;

        #endregion Private Fields

        #region Public Constructors

        public BaseTestFixture()
        {
            Mocker = new AutoMocker();
            Service = Mocker.CreateInstance<TService>();
            Repository = Mocker.GetMock<TRepository>();
        }

        #endregion Public Constructors

        #region Public Properties

        public AutoMocker Mocker { get; private set; }

        public Mock<TRepository> Repository { get; private set; }

        public TService Service { get; private set; }

        #endregion Public Properties

        #region Public Methods

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion Public Methods

        #region Private Methods

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    Service = null;
                    Mocker = null;
                }
                _disposed = true;
            }
        }

        #endregion Private Methods
    }
}
