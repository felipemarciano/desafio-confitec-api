﻿using System;

namespace DesafioConfitec.Tests.Core
{
    [AttributeUsage(AttributeTargets.Method)]
    public class TestPriorityAttribute : Attribute
    {
        #region Public Constructors

        public TestPriorityAttribute(int priority)
        {
            Priority = priority;
        }

        #endregion Public Constructors

        #region Public Properties

        public int Priority { get; }

        #endregion Public Properties
    }
}