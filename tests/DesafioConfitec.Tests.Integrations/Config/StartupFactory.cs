﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Hosting;
using System.Diagnostics.CodeAnalysis;

namespace DesafioConfitec.Tests.Integrations.Config
{
    [ExcludeFromCodeCoverage]
    public class StartupFactory<TStartup> where TStartup : class
    {
        #region Public Methods

        public static TestServer CreateHost()
        {
            var webHost = WebHost.CreateDefaultBuilder()
                   .UseSetting("detailedErrors", "true")
                   .UseKestrel()
                   .UseIISIntegration()
                   .CaptureStartupErrors(true)
                   .UseEnvironment("Staging")
                   .UseStartup<TStartup>();

            var server = new TestServer(webHost);
            return server;
        }

        #endregion Public Methods
    }
}