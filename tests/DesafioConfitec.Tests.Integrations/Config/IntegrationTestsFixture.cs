﻿using DesafioConfitec.API;
using DesafioConfitec.Dominio.Dto;
using DesafioConfitec.Dominio.Notify;
using DesafioConfitec.Http.Core.Extensions;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace DesafioConfitec.Tests.Integrations.Config
{
    [ExcludeFromCodeCoverage]
    [CollectionDefinition(nameof(IntegrationApiTestsFixtureCollection))]
    public class IntegrationApiTestsFixtureCollection : ICollectionFixture<IntegrationTestsFixture<StartupStaging>> { }

    [ExcludeFromCodeCoverage]
    public class IntegrationTestsFixture<TStartup> : IDisposable where TStartup : class
    {
        #region Public Fields

        public readonly ApplicationSettings ApplicationSettings;
        public readonly HttpClient Client;
        public readonly TestServer Factory;
        public string UsuarioEmail;
        public string UsuarioSenha;
        public string UsuarioToken;

        #endregion Public Fields

        #region Public Constructors

        public IntegrationTestsFixture()
        {
            Factory = StartupFactory<TStartup>.CreateHost();
            Client = Factory.CreateClient();
            Configuration = (IConfiguration)Factory.Services.GetService(typeof(IConfiguration));

            ApplicationSettings = new ApplicationSettings();
            Configuration.Bind(nameof(ApplicationSettings), ApplicationSettings);
        }

        #endregion Public Constructors

        #region Public Properties

        public IConfiguration Configuration { get; }

        #endregion Public Properties

        #region Public Methods

        public void Dispose()
        {
            Factory.Dispose();
        }

        public void GerarUserSenhaOk()
        {
            UsuarioEmail = ApplicationSettings.UsuarioEmail;
            UsuarioSenha = ApplicationSettings.UsuarioSenha;
        }

        public async Task<Notification<TokenDto>> RealizarLoginApiAsync()
        {
            var userData = new
            {
                Email = UsuarioEmail,
                Senha = UsuarioSenha
            };

            var response = await Client.PostAsJsonAsync("api/v1/account/login", userData).ConfigureAwait(false);
            var notification = await response.Content.ReadAsJsonAsync<Notification<TokenDto>>().ConfigureAwait(false);

            UsuarioToken = notification.Data?.Token;

            return notification;
        }

        #endregion Public Methods
    }
}