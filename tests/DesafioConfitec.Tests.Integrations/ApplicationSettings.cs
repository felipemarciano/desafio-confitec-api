﻿using System.Diagnostics.CodeAnalysis;

namespace DesafioConfitec.Tests.Integrations
{
    [ExcludeFromCodeCoverage]
    public class ApplicationSettings
    {
        #region Public Properties

        public string UrlAccountLogin { get; set; }

        public string UrlObterUsuarioPorId { get; set; }

        public string UsuarioEmail { get; set; }

        public string UsuarioSenha { get; set; }

        #endregion Public Properties
    }
}