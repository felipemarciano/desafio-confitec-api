﻿using Bogus;
using DesafioConfitec.API;
using DesafioConfitec.Dominio.Dto;
using DesafioConfitec.Dominio.Entities;
using DesafioConfitec.Dominio.Notify;
using DesafioConfitec.Http.Core.Extensions;
using DesafioConfitec.Tests.Integrations.Config;
using System.Threading.Tasks;
using Xunit;

namespace DesafioConfitec.Tests.Integrations.Usuarios
{
    [Collection(nameof(IntegrationApiTestsFixtureCollection))]
    public class ObterUsuarioTest
    {
        #region Private Fields

        private readonly IntegrationTestsFixture<StartupStaging> _testsFixture;

        #endregion Private Fields

        #region Public Constructors

        public ObterUsuarioTest(IntegrationTestsFixture<StartupStaging> testsFixture)
        {
            _testsFixture = testsFixture;
        }

        #endregion Public Constructors

        #region Public Methods

        [Trait(nameof(Usuario), "Teste de integração com API")]
        [Fact(DisplayName = "Obter usuario com err0 404 - Não encontrado")]
        public async Task ObterUsuario_DeveRetornarComErro404Async()
        {
            var id = new Faker().Random.Int();

            _testsFixture.GerarUserSenhaOk();
            await _testsFixture.RealizarLoginApiAsync().ConfigureAwait(false);
            var getResponse = await _testsFixture.Client.GetAsJsonAsync($"{_testsFixture.ApplicationSettings.UrlObterUsuarioPorId}{id}", _testsFixture.UsuarioToken);

            Assert.True(getResponse.StatusCode == System.Net.HttpStatusCode.NotFound);
        }

        [Trait(nameof(Usuario), "Teste de integração com API")]
        [Fact(DisplayName = "Obter usuario com sucesso")]
        public async Task ObterUsuario_DeveRetornarComSucessoAsync()
        {
            _testsFixture.GerarUserSenhaOk();
            await _testsFixture.RealizarLoginApiAsync().ConfigureAwait(false);
            var getResponse = await _testsFixture.Client.GetAsJsonAsync($"{_testsFixture.ApplicationSettings.UrlObterUsuarioPorId}{2}", _testsFixture.UsuarioToken);
            var result = await getResponse.Content.ReadAsJsonAsync<Notification<UsuarioDto>>().ConfigureAwait(false);

            Assert.True(result.Success);
        }

        [Trait(nameof(Usuario), "Teste de integração com API")]
        [Fact(DisplayName = "Obter usuario erro 401 - Não autorizado")]
        public async Task ObterUsuario_Erro401Async()
        {
            var id = new Faker().Random.Int();

            var getResponse = await _testsFixture.Client.GetAsJsonAsync($"{_testsFixture.ApplicationSettings.UrlObterUsuarioPorId}{id}");

            Assert.True(getResponse.StatusCode == System.Net.HttpStatusCode.Unauthorized);
        }

        #endregion Public Methods
    }
}