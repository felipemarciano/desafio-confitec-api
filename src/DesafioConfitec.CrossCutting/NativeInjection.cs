﻿using AutoMapper;
using DesafioConfitec.Data.Context;
using DesafioConfitec.Data.Repositories;
using DesafioConfitec.Data.UoW;
using DesafioConfitec.Dominio.Interfaces.Repositories;
using DesafioConfitec.Dominio.Interfaces.Services;
using DesafioConfitec.Dominio.Notify;
using DesafioConfitec.Dominio.Security;
using DesafioConfitec.Dominio.Services;
using DesafioConfitec.Http.Core.Interfaces;
using DesafioConfitec.Http.Core.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace DesafioConfitec.CrossCutting
{
    public static class NativeInjection
    {
        #region Public Methods

        public static void RegisterDomainServices(this IServiceCollection services)
        {
            services.AddScoped<IEscolaridadeService, EscolaridadeService>();
            services.AddScoped<IUsuarioService, UsuarioService>();
        }

        public static void RegisterOtherServices(this IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IdentityUser>();
            services.AddScoped<ConfitecContext>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped(typeof(IRepositoryBase<>), typeof(RepositoryBase<>));
            services.AddTransient(typeof(Notification<>));
            services.AddScoped<IMapper>(sp => new Mapper(sp.GetRequiredService<IConfigurationProvider>(), sp.GetService));
            services.AddScoped<IHttpClientService, HttpClientService>();
        }

        public static void RegisterRepositoriesServices(this IServiceCollection services)
        {
            services.AddScoped<IEscolaridadeRepository, EscolaridadeRepository>();
            services.AddScoped<IUsuarioRepository, UsuarioRepository>();
        }

        #endregion Public Methods
    }
}