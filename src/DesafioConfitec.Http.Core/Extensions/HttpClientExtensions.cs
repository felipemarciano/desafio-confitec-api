﻿using Newtonsoft.Json;
using System.Diagnostics.CodeAnalysis;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace DesafioConfitec.Http.Core.Extensions
{
    [ExcludeFromCodeCoverage]
    public static class HttpClientExtensions
    {
        #region Public Methods

        public static StringContent AddApplicationJson(this StringContent content)
        {
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json")
            {
                CharSet = "UTF-8"
            };
            return content;
        }

        public static async Task<HttpResponseMessage> GetAsJsonAsync(this HttpClient httpClient, string url, string token = "")
        {
            Configure(httpClient, token);
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = await httpClient.GetAsync(url).ConfigureAwait(false);

            return response;
        }

        public static async Task<HttpResponseMessage> PostAsJsonAsync<T>(this HttpClient httpClient, string url, T data, string token = "")
        {
            Configure(httpClient, token);

            var dataAsString = JsonConvert.SerializeObject(data);
            var content = new StringContent(dataAsString).AddApplicationJson();

            return await httpClient.PostAsync(url, content).ConfigureAwait(false);
        }

        public static async Task<HttpResponseMessage> PutAsJsonAsync<T>(this HttpClient httpClient, string url, T data, string token = "")
        {
            Configure(httpClient, token);

            var dataAsString = JsonConvert.SerializeObject(data);
            var content = new StringContent(dataAsString).AddApplicationJson();

            return await httpClient.PutAsync(url, content).ConfigureAwait(false);
        }

        public static async Task<T> ReadAsJsonAsync<T>(this HttpContent content)
        {
            var dataAsString = await content.ReadAsStringAsync().ConfigureAwait(false);

            return JsonConvert.DeserializeObject<T>(dataAsString);
        }

        #endregion Public Methods

        #region Private Methods

        private static void Configure(HttpClient httpClient, string token)
        {
            httpClient.DefaultRequestHeaders.Clear();
            if (!string.IsNullOrWhiteSpace(token))
            {
                httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            }
        }

        #endregion Private Methods
    }
}