﻿using DesafioConfitec.Http.Core.Extensions;
using DesafioConfitec.Http.Core.Interfaces;
using System.Diagnostics.CodeAnalysis;
using System.Net.Http;
using System.Threading.Tasks;

namespace DesafioConfitec.Http.Core.Services
{
    [ExcludeFromCodeCoverage]
    public class HttpClientService : IHttpClientService
    {
        #region Private Fields

        private readonly HttpClient _httpClient;

        #endregion Private Fields

        #region Public Constructors

        public HttpClientService()
        {
            _httpClient = new HttpClient();
        }

        #endregion Public Constructors

        #region Public Methods

        public async Task<HttpResponseMessage> GetAsJsonAsync(string url, string token = "")
        {
            return await _httpClient.GetAsJsonAsync(url, token);
        }

        public async Task<HttpResponseMessage> PostAsJsonAsync<T>(string url, T data, string token = "")
        {
            return await _httpClient.PostAsJsonAsync(url, data, token).ConfigureAwait(false);
        }

        public async Task<HttpResponseMessage> PutAsJsonAsync<T>(string url, T data, string token = "")
        {
            return await _httpClient.PutAsJsonAsync(url, data, token).ConfigureAwait(false);
        }

        #endregion Public Methods
    }
}