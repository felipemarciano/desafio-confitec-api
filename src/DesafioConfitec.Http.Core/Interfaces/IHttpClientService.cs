﻿using System.Net.Http;
using System.Threading.Tasks;

namespace DesafioConfitec.Http.Core.Interfaces
{
    public interface IHttpClientService
    {
        #region Public Methods

        Task<HttpResponseMessage> GetAsJsonAsync(string url, string token = "");

        Task<HttpResponseMessage> PostAsJsonAsync<T>(string url, T data, string token = "");

        Task<HttpResponseMessage> PutAsJsonAsync<T>(string url, T data, string token = "");

        #endregion Public Methods
    }
}