﻿using DesafioConfitec.Dominio.Enums;
using System;

namespace DesafioConfitec.Dominio.Filters
{
    public class UsuarioFilter
    {
        #region Public Properties

        public DateTime? DataNascimentoInicio { get; set; }

        public DateTime? DataNascimentoTermino { get; set; }

        public string Email { get; set; }

        public EnumEscolaridade? Escolaridade { get; set; }

        public string Nome { get; set; }

        public string Sobrenome { get; set; }

        #endregion Public Properties
    }
}