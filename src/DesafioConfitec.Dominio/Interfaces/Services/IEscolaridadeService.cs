﻿using DesafioConfitec.Dominio.Dto;
using DesafioConfitec.Dominio.Notify;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DesafioConfitec.Dominio.Interfaces.Services
{
    public interface IEscolaridadeService : IDisposable
    {
        #region Public Methods

        Task<Notification<IEnumerable<EscolaridadeDto>>> ObterEscolaridadesAsync();

        #endregion Public Methods
    }
}