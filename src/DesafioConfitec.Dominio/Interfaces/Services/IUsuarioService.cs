﻿using DesafioConfitec.Dominio.Dto;
using DesafioConfitec.Dominio.Entities;
using DesafioConfitec.Dominio.Filters;
using DesafioConfitec.Dominio.Notify;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DesafioConfitec.Dominio.Interfaces.Services
{
    public interface IUsuarioService : IDisposable
    {
        #region Public Methods

        Task<Notification<UsuarioCadastroDto>> AdicionarAsync(UsuarioCadastroDto usuarioDto);

        Task<Notification<UsuarioAlteracaoDto>> EditarAsync(UsuarioAlteracaoDto usuarioDto);

        Task<Notification<object>> ExcluirAsync(int id);

        Task<Notification<Usuario>> LoginPorEmailAsync(AutenticacaoUsuarioDto autenticacaoUsuarioDto);

        Task<Notification<UsuarioDto>> ObterPorIdAsync(int id);

        Task<Notification<IEnumerable<UsuarioDto>>> PesquisarAsync(UsuarioFilter filter);

        #endregion Public Methods
    }
}