﻿using DesafioConfitec.Dominio.Dto;
using DesafioConfitec.Dominio.Entities;
using DesafioConfitec.Dominio.Filters;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DesafioConfitec.Dominio.Interfaces.Repositories
{
    public interface IUsuarioRepository : IRepositoryBase<Usuario>, IDisposable
    {
        #region Public Methods

        Task<Usuario> ObterPorEmailAsync(string email);

        Task<Usuario> ObterPorIdAsync(int id);

        Task<Usuario> ObterPorIdComSenhaAsync(int id);

        Task<IEnumerable<UsuarioDto>> PesquisarAsync(UsuarioFilter filter);

        Task<bool> VerificarDuplicidadeAsync(int id, string nome, string sobrenome, string email);

        #endregion Public Methods
    }
}