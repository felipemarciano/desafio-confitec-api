﻿using DesafioConfitec.Dominio.Dto;
using DesafioConfitec.Dominio.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DesafioConfitec.Dominio.Interfaces.Repositories
{
    public interface IEscolaridadeRepository : IRepositoryBase<Escolaridade>, IDisposable
    {
        #region Public Methods

        Task<IEnumerable<EscolaridadeDto>> ObterEscolaridadesAsync();

        #endregion Public Methods
    }
}