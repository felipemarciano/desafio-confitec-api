﻿namespace DesafioConfitec.Dominio.Interfaces.Repositories
{
    public interface IRepositoryBase<T> where T : class
    {
        #region Public Methods

        void Adicionar(T objeto);

        void Editar(T objeto);

        void Excluir(T objeto);

        #endregion Public Methods
    }
}