﻿using DesafioConfitec.Dominio.Extensions;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace DesafioConfitec.Dominio.Security
{
    public class IdentityUser
    {
        #region Private Fields

        private readonly IHttpContextAccessor _httpContextAccessor;

        #endregion Private Fields

        #region Public Constructors

        public IdentityUser(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        #endregion Public Constructors

        #region Public Properties

        public IEnumerable<Claim> Claims => _httpContextAccessor.HttpContext.User.Claims;

        public string Email => _httpContextAccessor.HttpContext.User.Identity.Name;

        public DateTime? ExpiraEm => _httpContextAccessor.HttpContext.User.ObterDataExpiracao();

        public int Id => _httpContextAccessor.HttpContext.User.ObterUsuarioId();

        public bool IsAutenticated => _httpContextAccessor.HttpContext.User.Identity.IsAuthenticated;

        public string Nome => _httpContextAccessor.HttpContext.User.ObterNome();

        #endregion Public Properties
    }
}