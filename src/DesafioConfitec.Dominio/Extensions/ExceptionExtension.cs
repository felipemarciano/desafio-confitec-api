﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DesafioConfitec.Dominio.Extensions
{
    public static class ExceptionExtension
    {
        #region Public Methods

        public static IList<string> GetAllMessagesException(this Exception exception)
        {
            var listaErros = exception.GetInnerExceptions()
               .Where(x => !string.IsNullOrWhiteSpace(x.Message))
               .Select(x => x.Message)
               .Distinct()
               .ToList();

            return listaErros;
        }

        public static IEnumerable<Exception> GetInnerExceptions(this Exception ex)
        {
            if (ex != null)
            {
                var exception = ex;
                do
                {
                    yield return exception;
                    exception = exception.InnerException;
                }
                while (exception != null);
            }
        }

        #endregion Public Methods
    }
}