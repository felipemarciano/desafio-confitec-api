﻿using DesafioConfitec.Dominio.ValuesObjects;

namespace DesafioConfitec.Dominio.Extensions
{
    public static class StringExtension
    {
        #region Public Methods

        public static bool EmailObrigatorioValido(this string email)
        {
            var result = new EmailVo(email);
            return result.IsValid;
        }

        #endregion Public Methods
    }
}