﻿using System;
using System.Security.Claims;

namespace DesafioConfitec.Dominio.Extensions
{
    public static class ClaimsPrincipalExtensions
    {
        #region Public Methods

        public static DateTime? ObterDataExpiracao(this ClaimsPrincipal principal)
        {
            if (principal == null)
            {
                throw new ArgumentException(nameof(principal));
            }

            var claim = principal.FindFirst("exp");
            if (DateTime.TryParse(claim?.Value, out DateTime novaData))
            {
                return novaData;
            }
            return novaData;
        }

        public static string ObterNome(this ClaimsPrincipal principal)
        {
            if (principal == null)
            {
                throw new ArgumentException(nameof(principal));
            }

            var claim = principal.FindFirst("nome");
            return claim?.Value;
        }

        public static int ObterUsuarioId(this ClaimsPrincipal principal)
        {
            if (principal == null)
            {
                throw new ArgumentException(nameof(principal));
            }

            var claim = principal.FindFirst("usuario_id");
            return string.IsNullOrWhiteSpace(claim?.Value) ? 0 : Int32.Parse(claim.Value);
        }

        #endregion Public Methods
    }
}
