﻿using System;

namespace DesafioConfitec.Dominio.Extensions
{
    public static class EnumExtension
    {
        #region Public Methods

        public static string GetValueString(this Enum enumerador)
        {
            return enumerador.GetHashCode().ToString();
        }

        public static bool IsValid(this Enum enumerador)
        {
            return enumerador != null && int.Parse(enumerador.GetHashCode().ToString()) > 0;
        }

        #endregion Public Methods
    }
}