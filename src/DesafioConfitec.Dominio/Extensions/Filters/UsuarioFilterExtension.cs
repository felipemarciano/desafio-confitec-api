﻿using DesafioConfitec.Dominio.Filters;
using DesafioConfitec.Dominio.Notify;

namespace DesafioConfitec.Dominio.Extensions.Filters
{
    public static class UsuarioFilterExtension
    {
        #region Public Methods

        public static Notification<object> Validate(this UsuarioFilter filter)
        {
            var notification = new Notification<object>();

            if (filter.DataNascimentoInicio.IsValid() || filter.DataNascimentoTermino.IsValid())
            {
                var periodoNascimento = filter.DataNascimentoInicio.ValidarPeriodo(filter.DataNascimentoTermino);
                notification.AddError(periodoNascimento.Result);
            }

            return notification;
        }

        #endregion Public Methods
    }
}