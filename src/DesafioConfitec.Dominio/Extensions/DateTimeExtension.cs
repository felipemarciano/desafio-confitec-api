﻿using DesafioConfitec.Dominio.Validators;
using System;

namespace DesafioConfitec.Dominio.Extensions
{
    public static class DateTimeExtension
    {
        #region Public Methods

        public static bool IsValid(this DateTime? data)
        {
            if (!data.HasValue)
                return false;

            return ValidarData(data);
        }

        public static bool IsValid(this DateTime data)
        {
            return ValidarData(data);
        }

        public static PeriodoValidator ValidarPeriodo(this DateTime? dataInicio, DateTime? dataTermino)
        {
            var periodo = new PeriodoValidator(dataInicio, dataTermino);
            return periodo;
        }

        #endregion Public Methods

        #region Private Methods

        private static bool ValidarData(DateTime? data)
        {
            if (data.Value == DateTime.MinValue ||
                data.Value == DateTime.MaxValue)
                return false;

            return true;
        }

        #endregion Private Methods
    }
}