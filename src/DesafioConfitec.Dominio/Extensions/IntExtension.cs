﻿namespace DesafioConfitec.Dominio.Extensions
{
    public static class IntExtension
    {
        #region Public Methods

        public static bool IsValid(this int number)
        {
            return number > 0;
        }

        public static bool IsValid(this int? number)
        {
            return number.HasValue && number.Value > 0;
        }

        #endregion Public Methods
    }
}