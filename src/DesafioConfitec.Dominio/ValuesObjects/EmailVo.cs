﻿using FluentValidation;

namespace DesafioConfitec.Dominio.ValuesObjects
{
    public class EmailVo : AbstractValidator<EmailVo>
    {
        #region Public Constructors

        public EmailVo(string email)
        {
            Email = email;

            var message = "Informe um email válido";

            RuleFor(x => x.Email)
                .NotNull().WithMessage(message)
                .NotEmpty().WithMessage(message)
                .EmailAddress().WithMessage(message);

            IsValid = Validate(this).IsValid;
        }

        #endregion Public Constructors

        #region Public Properties

        public string Email { get; private set; }

        public bool IsValid { get; private set; }

        #endregion Public Properties
    }
}