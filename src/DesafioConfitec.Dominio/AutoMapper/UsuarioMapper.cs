﻿using DesafioConfitec.Dominio.Dto;
using DesafioConfitec.Dominio.Entities;
using System.Collections.Generic;

namespace DesafioConfitec.Dominio.AutoMapper
{
    public static class UsuarioMapper
    {
        #region Private Fields

        private static readonly BaseConfigurationMapper<Usuario, UsuarioDto> _configurationMapper;

        #endregion Private Fields

        #region Public Constructors

        static UsuarioMapper()
        {
            _configurationMapper = new BaseConfigurationMapper<Usuario, UsuarioDto>();
        }

        #endregion Public Constructors

        #region Public Methods

        public static Usuario ToEntity(this UsuarioDto obj)
        {
            return _configurationMapper.ToEntity(obj);
        }

        public static IEnumerable<Usuario> ToEntity(this IEnumerable<UsuarioDto> obj)
        {
            return _configurationMapper.ToEntity(obj);
        }

        public static UsuarioDto ToResult(this Usuario obj)
        {
            return _configurationMapper.ToResult(obj);
        }

        public static IEnumerable<UsuarioDto> ToResult(this IEnumerable<Usuario> obj)
        {
            return _configurationMapper.ToResult(obj);
        }

        #endregion Public Methods
    }
}