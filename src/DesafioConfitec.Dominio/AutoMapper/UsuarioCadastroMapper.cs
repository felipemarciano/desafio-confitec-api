﻿using DesafioConfitec.Dominio.Dto;
using DesafioConfitec.Dominio.Entities;

namespace DesafioConfitec.Dominio.AutoMapper
{
    public static class UsuarioCadastroMapper
    {
        #region Private Fields

        private static readonly BaseConfigurationMapper<Usuario, UsuarioCadastroDto> _configurationMapper;

        #endregion Private Fields

        #region Public Constructors

        static UsuarioCadastroMapper()
        {
            _configurationMapper = new BaseConfigurationMapper<Usuario, UsuarioCadastroDto>();
        }

        #endregion Public Constructors

        #region Public Methods

        public static Usuario ToEntity(this UsuarioCadastroDto obj)
        {
            return _configurationMapper.ToEntity(obj);
        }

        #endregion Public Methods
    }
}