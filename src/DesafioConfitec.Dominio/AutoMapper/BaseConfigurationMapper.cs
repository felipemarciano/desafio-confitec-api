﻿using AutoMapper;
using System.Collections.Generic;

namespace DesafioConfitec.Dominio.AutoMapper
{
    public class BaseConfigurationMapper<TEntity, TResult>
        where TEntity : class
        where TResult : class
    {
        #region Private Fields

        private readonly IMapper _mapper;

        #endregion Private Fields

        #region Public Constructors

        public BaseConfigurationMapper(IMapper mapper = null)
        {
            _mapper = mapper ?? CriarConfiguracaoAutoMapper();
        }

        #endregion Public Constructors

        #region Public Methods

        public IMapper CriarConfiguracaoAutoMapper()
        {
            var mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TEntity, TResult>().ReverseMap();
            });
            return mapper.CreateMapper();
        }

        public TEntity ToEntity(TResult obj) => _mapper.Map<TResult, TEntity>(obj);

        public IList<TEntity> ToEntity(IList<TResult> obj) => _mapper.Map<IList<TResult>, IList<TEntity>>(obj);

        public IEnumerable<TEntity> ToEntity(IEnumerable<TResult> obj) => _mapper.Map<IEnumerable<TResult>, IEnumerable<TEntity>>(obj);

        public ISet<TEntity> ToEntity(ISet<TResult> list)
        {
            var entity = new HashSet<TEntity>();
            foreach (var item in list)
            {
                entity.Add(ToEntity(item));
            }

            return entity;
        }

        public TResult ToResult(TEntity obj) => _mapper.Map<TEntity, TResult>(obj);

        public IList<TResult> ToResult(IList<TEntity> obj) => _mapper.Map<IList<TEntity>, IList<TResult>>(obj);

        public IEnumerable<TResult> ToResult(IEnumerable<TEntity> obj) => _mapper.Map<IEnumerable<TEntity>, IEnumerable<TResult>>(obj);

        public ISet<TResult> ToResult(ISet<TEntity> list)
        {
            var result = new HashSet<TResult>();
            foreach (var item in list)
            {
                result.Add(ToResult(item));
            }

            return result;
        }

        #endregion Public Methods
    }
}