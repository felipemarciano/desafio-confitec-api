﻿using DesafioConfitec.Dominio.Entities;
using DesafioConfitec.Dominio.Enums;
using FluentValidation;
using FluentValidation.Results;

namespace DesafioConfitec.Dominio.Validators
{
    public class UsuarioValidator : AbstractValidator<Usuario>
    {
        #region Public Fields

        public static string MensagemDataNascimentoForaDoRange = "Informe a data de nascimento maior ou igual a 01/01/1920 e menor ou igual a data atual!";
        public static string MensagemDataNascimentoInvalida = "Data de nascimento inválida!";
        public static string MensagemEmailMaximoCaractereres = "Informe um email com até 100 caracteres!";
        public static string MensagemEmailInvalido = "Email inválido!";
        public static string MensagemEscolaridadeInvalida = "Escolaridade inválida!";
        public static string MensagemNomeInvalido = "Informe um nome válido!";
        public static string MensagemNomeMaximoCaracteres = "Informe um nome com até 60 caracteres!";
        public static string MensagemSenhaInvalida = "Informe uma senha!";
        public static string MensagemSenhaMaximoCaracteres = "Informe no máximo 10 caracteres para a senha!";
        public static string MensagemSenhaMinimoCaracteres = "Informe no mínimo 4 caracteres para senha!";
        public static string MensagemSenhasNaoConferem = "As senhas não conferem!";
        public static string MensagemSobrenomeInvalido = "Informe um sobrenome válido!";
        public static string MensagemSobrenomeMaximoCaracteres = "Informe um sobrenome com até 60 caracteres!";

        #endregion Public Fields

        #region Public Constructors

        public UsuarioValidator(Usuario usuario, bool validarSenha, string confirmacaoSenha = "")
        {
            RuleFor(x => x.Email)
                .NotEmpty().WithMessage(MensagemEmailInvalido)
                .EmailAddress().WithMessage(MensagemEmailInvalido)
                .MaximumLength(100).WithMessage(MensagemEmailMaximoCaractereres);

            RuleFor(x => x.DataNascimento)
                .SetValidator(new DataNascimentoValidator());

            RuleFor(x => x.Escolaridade)
                .InclusiveBetween(EnumEscolaridade.Infantil.GetHashCode(), EnumEscolaridade.Superior.GetHashCode())
                .WithMessage(MensagemEscolaridadeInvalida);

            RuleFor(x => x.Nome)
                .NotEmpty().WithMessage(MensagemNomeInvalido)
                .MaximumLength(60).WithMessage(MensagemNomeMaximoCaracteres);

            RuleFor(x => x.Sobrenome)
                .NotEmpty().WithMessage(MensagemSobrenomeInvalido)
                .MaximumLength(60).WithMessage(MensagemSobrenomeMaximoCaracteres);

            if (validarSenha)
            {
                RuleFor(x => x.Senha)
                    .NotEmpty().WithMessage(MensagemSenhaInvalida)
                    .MinimumLength(4).WithMessage(MensagemSenhaMinimoCaracteres)
                    .MaximumLength(10).WithMessage(MensagemSenhaMaximoCaracteres);

                RuleFor(x => x.Senha)
                    .Equal(confirmacaoSenha).WithMessage(MensagemSenhasNaoConferem);
            }

            ValidationResult = Validate(usuario);
        }

        #endregion Public Constructors

        #region Public Properties

        public ValidationResult ValidationResult { get; private set; }

        #endregion Public Properties
    }
}