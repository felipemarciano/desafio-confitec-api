﻿using FluentValidation;
using FluentValidation.Results;
using System;

namespace DesafioConfitec.Dominio.Validators
{
    public class PeriodoValidator : AbstractValidator<PeriodoValidator>
    {
        #region Public Constructors

        public PeriodoValidator(DateTime? dataInicio, DateTime? dataTermino)
        {
            DataInicio = dataInicio;
            DataTermino = dataTermino;

            Validar();
        }

        #endregion Public Constructors

        #region Public Properties

        public DateTime? DataInicio { get; private set; }

        public DateTime? DataTermino { get; private set; }

        public ValidationResult Result { get; private set; }

        #endregion Public Properties

        #region Private Methods

        private void Validar()
        {
            RuleFor(x => DataInicio)
                .NotNull().WithMessage("Informe a data de início!");

            RuleFor(x => DataTermino)
                .NotNull().WithMessage("Informe a data de término!");

            Result = Validate(this);
            if (!Result.IsValid)
            {
                return;
            }

            RuleFor(x => x.DataInicio)
                .LessThanOrEqualTo(DataTermino.Value).WithMessage("A data de início não pode ser maior do que a data de término!");

            RuleFor(x => ((DataInicio.Value - DateTime.Now.AddDays(-367)).Days * (-1)))
                .LessThanOrEqualTo(366).WithMessage("A data de início não pode ser maior do que 1 ano em relação a data atual!");

            Result = Validate(this);
        }

        #endregion Private Methods
    }
}