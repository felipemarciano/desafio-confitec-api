﻿using DesafioConfitec.Dominio.Extensions;
using FluentValidation.Results;
using FluentValidation.Validators;
using System;

namespace DesafioConfitec.Dominio.Validators
{
    public class DataNascimentoValidator : PropertyValidator
    {
        #region Public Properties

        public DateTime? Nascimento { get; private set; }

        #endregion Public Properties

        #region Public Methods

        public override string ToString()
        {
            return Nascimento.HasValue ? Nascimento.Value.ToString("dd/MM/yyyy") : null;
        }

        #endregion Public Methods

        #region Protected Methods

        protected override bool IsValid(PropertyValidatorContext context)
        {
            return SetValidation(context);
        }

        #endregion Protected Methods

        #region Private Methods

        private bool SetValidation(PropertyValidatorContext context, bool dataNascimentoObrigatoria = false)
        {
            var propertyValue = (DateTime?)context.PropertyValue;
            Nascimento = propertyValue;
            if (dataNascimentoObrigatoria || Nascimento.IsValid())
            {
                if (!Nascimento.IsValid())
                {
                    context.Rule.CurrentValidator.Options.SetErrorMessage("Data de nascimento inválida!");
                    return false;
                }

                var data1920 = new DateTime(1920, 1, 1);

                if (Nascimento.Value.Date < data1920 || Nascimento.Value.Date > DateTime.Now.Date)
                {
                    context.Rule.CurrentValidator.Options
                           .SetErrorMessage($"Informe a data de nascimento maior ou igual a {data1920.ToShortDateString()} e menor ou igual a data atual!");
                    return false;
                }
            }

            return true;
        }

        #endregion Private Methods
    }
}