﻿namespace DesafioConfitec.Dominio.Dto
{
    public class EscolaridadeDto
    {
        #region Public Properties

        public string Descricao { get; set; }

        public int Id { get; set; }

        #endregion Public Properties
    }
}