﻿using System;

namespace DesafioConfitec.Dominio.Dto
{
    public class TokenDto
    {
        #region Public Properties

        public DateTime ExpiraEm { get; set; }

        public string Token { get; set; }

        public UsuarioTokenDto Usuario { get; set; }

        #endregion Public Properties
    }
}