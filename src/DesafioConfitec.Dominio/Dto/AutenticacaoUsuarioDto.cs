﻿namespace DesafioConfitec.Dominio.Dto
{
    public class AutenticacaoUsuarioDto
    {
        #region Public Properties

        public string Email { get; set; }

        public string Senha { get; set; }

        #endregion Public Properties
    }
}