﻿using System;

namespace DesafioConfitec.Dominio.Dto
{
    public class UsuarioCadastroDto
    {
        #region Public Properties

        public string ConfirmacaoSenha { get; set; }

        public DateTime DataNascimento { get; set; }

        public string Email { get; set; }

        public int Escolaridade { get; set; }

        public string Nome { get; set; }

        public string Senha { get; set; }

        public string Sobrenome { get; set; }

        #endregion Public Properties
    }
}