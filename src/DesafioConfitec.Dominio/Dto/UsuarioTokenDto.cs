﻿namespace DesafioConfitec.Dominio.Dto
{
    public class UsuarioTokenDto
    {
        #region Public Properties

        public string Email { get; set; }

        public string Id { get; set; }

        public string Nome { get; set; }

        #endregion Public Properties
    }
}