﻿using System;

namespace DesafioConfitec.Dominio.Dto
{
    public class UsuarioAlteracaoDto
    {
        #region Public Properties

        public DateTime DataNascimento { get; set; }

        public string Email { get; set; }

        public int Escolaridade { get; set; }

        public int Id { get; set; }

        public string Nome { get; set; }

        public string Sobrenome { get; set; }

        #endregion Public Properties
    }
}