﻿using System;
using System.Text;

namespace DesafioConfitec.Dominio.Entities
{
    public class Usuario
    {
        #region Public Constructors

        public Usuario(string nome, string sobrenome, string email, string senha, DateTime dataNascimento, int escolaridade)
        {
            Nome = nome;
            Sobrenome = sobrenome;
            Senha = senha;
            Email = email;
            DataNascimento = dataNascimento;
            Escolaridade = escolaridade;
        }

        #endregion Public Constructors

        #region Protected Constructors

        protected Usuario()
        {
        }

        #endregion Protected Constructors

        #region Public Properties

        public DateTime DataNascimento { get; private set; }

        public string Email { get; private set; }

        public int Escolaridade { get; private set; }

        public virtual Escolaridade EscolaridadeNavigation { get; set; }

        public int Id { get; private set; }

        public string Nome { get; private set; }

        public string Senha { get; private set; }

        public string Sobrenome { get; private set; }

        #endregion Public Properties

        #region Public Methods

        public void Atualizar(string nome, string sobrenome, string email, DateTime dataNascimento, int escolaridade)
        {
            Nome = nome;
            Sobrenome = sobrenome;
            Email = email;
            DataNascimento = dataNascimento;
            Escolaridade = escolaridade;
        }

        public void EncriptarSenha()
        {
            Senha = EncryptPassword(Senha);
        }

        public bool SenhaValida(string senha)
        {
            return EncryptPassword(senha).Equals(Senha);
        }

        #endregion Public Methods

        #region Private Methods

        private static string EncryptPassword(string pass)
        {
            if (string.IsNullOrEmpty(pass)) return "";

            var password = (pass += "|8116B327-7EE6-47D5-900D-94D7CC191495");
            var md5 = System.Security.Cryptography.MD5.Create();
            var data = md5.ComputeHash(Encoding.Default.GetBytes(password));
            var sbString = new StringBuilder();
            foreach (var t in data)
                sbString.Append(t.ToString("x2"));

            return sbString.ToString();
        }

        #endregion Private Methods
    }
}