﻿using System.Collections.Generic;

namespace DesafioConfitec.Dominio.Entities
{
    public class Escolaridade
    {
        #region Public Constructors

        public Escolaridade()
        {
            Usuarios = new HashSet<Usuario>();
        }

        #endregion Public Constructors

        #region Public Properties

        public string Descricao { get; private set; }

        public int Id { get; private set; }

        public virtual ICollection<Usuario> Usuarios { get; protected set; }

        #endregion Public Properties
    }
}