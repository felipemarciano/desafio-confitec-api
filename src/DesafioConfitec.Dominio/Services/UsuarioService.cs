﻿using DesafioConfitec.Dominio.AutoMapper;
using DesafioConfitec.Dominio.Dto;
using DesafioConfitec.Dominio.Entities;
using DesafioConfitec.Dominio.Extensions;
using DesafioConfitec.Dominio.Extensions.Filters;
using DesafioConfitec.Dominio.Filters;
using DesafioConfitec.Dominio.Interfaces.Repositories;
using DesafioConfitec.Dominio.Interfaces.Services;
using DesafioConfitec.Dominio.Notify;
using DesafioConfitec.Dominio.Resources;
using DesafioConfitec.Dominio.Security;
using DesafioConfitec.Dominio.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DesafioConfitec.Dominio.Services
{
    public class UsuarioService : BaseService, IUsuarioService
    {
        #region Private Fields

        private readonly IUsuarioRepository _usuarioRepository;
        private readonly IdentityUser _user;
        private bool _disposed;

        #endregion Private Fields

        #region Public Constructors

        public UsuarioService(IUsuarioRepository usuarioRepository, IdentityUser user)
        {
            _usuarioRepository = usuarioRepository;
            _user = user;
        }

        #endregion Public Constructors

        #region Public Methods

        public async Task<Notification<UsuarioCadastroDto>> AdicionarAsync(UsuarioCadastroDto usuarioDto)
        {
            var notification = new Notification<UsuarioCadastroDto>();

            if (usuarioDto == null)
            {
                notification.AddError("Formulario", MensagensPadroes.DadosFormularioInvalidos);
                return notification;
            }

            var usuarioCadastrado = await _usuarioRepository.VerificarDuplicidadeAsync(0, usuarioDto.Nome, usuarioDto.Sobrenome, usuarioDto.Email).ConfigureAwait(false);
            if (usuarioCadastrado)
            {
                notification.AddError(nameof(Usuario), MensagensPadroes.CadastroExistente);
                return notification;
            }

            var usuario = new Usuario(usuarioDto.Nome, usuarioDto.Sobrenome, usuarioDto.Email, usuarioDto.Senha, usuarioDto.DataNascimento, usuarioDto.Escolaridade);
            var usuarioValidator = new UsuarioValidator(usuario, true, usuarioDto.ConfirmacaoSenha);
            notification.AddError(usuarioValidator.ValidationResult);

            if (!notification.Success)
                return notification;

            usuario.EncriptarSenha();

            _usuarioRepository.Adicionar(usuario);

            return notification.SetOk(null, MensagensPadroes.CadastradoSucesso, HttpStatusCode.Created.GetHashCode());
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public async Task<Notification<UsuarioAlteracaoDto>> EditarAsync(UsuarioAlteracaoDto usuarioDto)
        {
            var notification = new Notification<UsuarioAlteracaoDto>();

            if (usuarioDto == null)
            {
                notification.AddError("Formulario", MensagensPadroes.DadosFormularioInvalidos);
                return notification;
            }

            var usuarioCadastrado = await _usuarioRepository.VerificarDuplicidadeAsync(usuarioDto.Id, usuarioDto.Nome, usuarioDto.Sobrenome, usuarioDto.Email).ConfigureAwait(false);
            if (usuarioCadastrado)
            {
                notification.AddError(nameof(Usuario), MensagensPadroes.CadastroExistente);
                return notification;
            }

            var usuario = await _usuarioRepository.ObterPorIdComSenhaAsync(usuarioDto.Id).ConfigureAwait(false);
            if (usuario == null)
            {
                notification.AddError(nameof(Usuario), MensagensPadroes.CadastroInexistente, HttpStatusCode.NotFound.GetHashCode());
                return notification;
            }

            usuario.Atualizar(usuarioDto.Nome, usuarioDto.Sobrenome, usuarioDto.Email, usuarioDto.DataNascimento, usuarioDto.Escolaridade);
            var usuarioValidator = new UsuarioValidator(usuario, false);
            notification.AddError(usuarioValidator.ValidationResult);

            if (!notification.Success)
                return notification;

            _usuarioRepository.Editar(usuario);

            return notification.SetOk(null, MensagensPadroes.AlteradoSucesso);
        }

        public async Task<Notification<object>> ExcluirAsync(int id)
        {
            var notification = new Notification<object>();
            if (!id.IsValid())
            {
                notification.AddError(nameof(Usuario), MensagensPadroes.ObjetoOuListaNaoEncontrado, HttpStatusCode.NotFound.GetHashCode());
                return notification;
            }

            if(_user.Id == id)
            {
                notification.AddError(nameof(Usuario), "Não é permitido excluir seu próprio usuário!", HttpStatusCode.BadRequest.GetHashCode());
                return notification;
            }

            var usuario = await _usuarioRepository.ObterPorIdAsync(id).ConfigureAwait(false);
            if (usuario == null)
            {
                notification.AddError(nameof(Usuario), MensagensPadroes.CadastroInexistente, HttpStatusCode.NotFound.GetHashCode());
                return notification;
            }

            _usuarioRepository.Excluir(usuario);

            return notification.SetOk(MensagensPadroes.ExcluidoSucesso);
        }

        public async Task<Notification<Usuario>> LoginPorEmailAsync(AutenticacaoUsuarioDto autenticacaoUsuarioDto)
        {
            var notification = new Notification<Usuario>();
            if (autenticacaoUsuarioDto == null)
            {
                notification.AddError("Formulário", MensagensPadroes.UsuarioOuSenhaInvalidos);
                return notification;
            }

            if (!autenticacaoUsuarioDto.Email.EmailObrigatorioValido() && string.IsNullOrWhiteSpace(autenticacaoUsuarioDto.Senha))
            {
                notification.AddError(nameof(Usuario), MensagensPadroes.UsuarioOuSenhaInvalidos);
                return notification;
            }

            var usuario = await _usuarioRepository.ObterPorEmailAsync(autenticacaoUsuarioDto.Email);
            if (usuario == null)
            {
                notification.AddError(nameof(Usuario), MensagensPadroes.CadastroInexistente, HttpStatusCode.NotFound.GetHashCode());
                return notification;
            }

            var senhaValida = usuario.SenhaValida(autenticacaoUsuarioDto.Senha);
            if (!senhaValida)
            {
                notification.AddError(nameof(Usuario), MensagensPadroes.UsuarioOuSenhaInvalidos);
                return notification;
            }

            return notification.SetOk(usuario);
        }

        public async Task<Notification<UsuarioDto>> ObterPorIdAsync(int id)
        {
            var notification = new Notification<UsuarioDto>();
            if (!id.IsValid())
            {
                notification.AddError(nameof(Usuario), MensagensPadroes.ObjetoOuListaNaoEncontrado, HttpStatusCode.NotFound.GetHashCode());
                return notification;
            }

            var usuario = await _usuarioRepository.ObterPorIdAsync(id).ConfigureAwait(false);
            if (usuario == null)
            {
                notification.AddError(nameof(Usuario), MensagensPadroes.CadastroInexistente, HttpStatusCode.NotFound.GetHashCode());
                return notification;
            }

            return notification.SetOk(usuario.ToResult());
        }

        public async Task<Notification<IEnumerable<UsuarioDto>>> PesquisarAsync(UsuarioFilter filter)
        {
            var notification = new Notification<IEnumerable<UsuarioDto>>();
            var notificationFilter = filter.Validate();
            if (!notificationFilter.Success)
            {
                notification.AddError(notificationFilter.Errors);
                return notification;
            }

            var lista = await _usuarioRepository.PesquisarAsync(filter);
            if ((lista?.Count() ?? 0) == 0)
            {
                notification.AddError(nameof(Usuario), MensagensPadroes.ObjetoOuListaNaoEncontrado, HttpStatusCode.NotFound.GetHashCode());
                return notification;
            }

            return notification.SetOk(lista);
        }

        #endregion Public Methods

        #region Private Methods

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _usuarioRepository?.Dispose();
                }
                _disposed = true;
            }
        }

        #endregion Private Methods
    }
}