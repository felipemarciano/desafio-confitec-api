﻿using DesafioConfitec.Dominio.Dto;
using DesafioConfitec.Dominio.Entities;
using DesafioConfitec.Dominio.Interfaces.Repositories;
using DesafioConfitec.Dominio.Interfaces.Services;
using DesafioConfitec.Dominio.Notify;
using DesafioConfitec.Dominio.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DesafioConfitec.Dominio.Services
{
    public class EscolaridadeService : BaseService, IEscolaridadeService
    {
        #region Private Fields

        private readonly IEscolaridadeRepository _escolaridadeRepository;

        private bool _disposed;

        #endregion Private Fields

        #region Public Constructors

        public EscolaridadeService(IEscolaridadeRepository escolaridadeRepository)
        {
            _escolaridadeRepository = escolaridadeRepository;
        }

        #endregion Public Constructors

        #region Public Methods

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public async Task<Notification<IEnumerable<EscolaridadeDto>>> ObterEscolaridadesAsync()
        {
            var notification = new Notification<IEnumerable<EscolaridadeDto>>();

            var lista = await _escolaridadeRepository.ObterEscolaridadesAsync().ConfigureAwait(false); ;
            if ((lista?.Count() ?? 0) == 0)
            {
                notification.AddError(nameof(Escolaridade), MensagensPadroes.ObjetoOuListaNaoEncontrado, HttpStatusCode.NotFound.GetHashCode());
                return notification;
            }

            return notification.SetOk(lista);
        }

        #endregion Public Methods

        #region Private Methods

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _escolaridadeRepository?.Dispose();
                }
                _disposed = true;
            }
        }

        #endregion Private Methods
    }
}