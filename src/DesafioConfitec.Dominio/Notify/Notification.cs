﻿using DesafioConfitec.Dominio.Extensions;
using FluentValidation.Results;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace DesafioConfitec.Dominio.Notify
{
    [Serializable]
    [JsonObject(MemberSerialization.OptIn)]
    public sealed class Notification<T>
    {
        #region Public Constructors

        public Notification()
        {
            Errors = new List<string>();
        }

        #endregion Public Constructors

        #region Public Properties

        [JsonProperty]
        public T Data { get; private set; }

        [JsonProperty]
        public IList<string> Errors { get; set; }

        [JsonProperty]
        public string Message { get; private set; }

        [JsonProperty]
        public int Status { get; private set; }

        [JsonProperty]
        public bool Success => !Errors.Any();

        #endregion Public Properties

        #region Public Methods

        public void AddError(ValidationResult validation, int status = (int)HttpStatusCode.BadRequest)
        {
            var listErrors = validation.Errors.Select(x => new string($"{x.PropertyName}:{x.ErrorMessage}")).ToList();
            listErrors.ForEach(e =>
            {
                Add(e);
            });
            Status = status;
        }

        public void AddError(IList<string> listErrors, int status = (int)HttpStatusCode.BadRequest)
        {
            foreach (var e in listErrors)
            {
                Add(e);
            }
            Status = status;
        }

        public void AddError(string property, string message, int status = (int)HttpStatusCode.BadRequest)
        {
            var error = $"{property}:{message}";
            Add(error);
            Status = status;
        }

        public void AddError(Exception exception)
        {
            AddException(exception);
            Status = (int)HttpStatusCode.InternalServerError;
        }

        public Notification<T> SetOk(T data, int status = (int)HttpStatusCode.OK)
        {
            Data = data;
            Status = status;

            return this;
        }

        public Notification<T> SetOk()
        {
            Status = (int)HttpStatusCode.OK;

            return this;
        }

        public Notification<T> SetOk(string message, int status = (int)HttpStatusCode.OK)
        {
            Message = message;
            Status = status;

            return this;
        }

        public Notification<T> SetOk(T data, string message, int status = (int)HttpStatusCode.OK)
        {
            Data = data;
            Status = status;
            Message = message;

            return this;
        }

        public override string ToString()
        {
            return string.Join(", ", Errors);
        }

        #endregion Public Methods

        #region Private Methods

        private void Add(string error)
        {
            if (!Errors.Contains(error))
            {
                Errors.Add(error);
            }
        }

        private void AddException(Exception exception)
        {
            var erros = exception.GetAllMessagesException();
            AddError(erros);
        }

        #endregion Private Methods
    }
}