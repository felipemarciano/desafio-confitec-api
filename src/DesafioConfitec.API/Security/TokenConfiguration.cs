﻿using DesafioConfitec.Dominio.Dto;
using DesafioConfitec.Dominio.Entities;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace DesafioConfitec.API.Security
{
    public class TokenConfiguration
    {
        #region Private Fields

        private readonly IConfiguration _configuration;

        #endregion Private Fields

        #region Public Constructors

        public TokenConfiguration(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        #endregion Public Constructors

        #region Public Properties

        public string Authority => _configuration.GetSection("TokenConfiguration:Authority").Value ?? throw new ArgumentException("Authority não informado!");

        public string ClientId => _configuration.GetSection("TokenConfiguration:ClientId").Value ?? throw new ArgumentException("ClientId não informado!");

        public string Emissor => _configuration.GetSection("TokenConfiguration:Emissor").Value ?? throw new ArgumentException("Emissor não informado!");

        public int ExpiracaoHoras => !string.IsNullOrWhiteSpace(_configuration.GetSection("TokenConfiguration:ExpiracaoHoras").Value) ?
                                                               Convert.ToInt32(_configuration.GetSection("TokenConfiguration:ExpiracaoHoras").Value)
                                                               : throw new ArgumentException("ExpiracaoHoras não informado!");

        public DateTime IssuedAt => DateTime.UtcNow;

        public DateTime NotBefore => DateTime.UtcNow;

        public string ClientSecret => _configuration.GetSection("TokenConfiguration:ClientSecret").Value ?? throw new ArgumentException("ClientSecret não informado!");

        #endregion Public Properties

        #region Public Methods

        public TokenDto GerarJwt(Usuario usuario)
        {
            var expiraEm = DateTime.UtcNow.AddHours(ExpiracaoHoras);
            var tokenHandler = new JwtSecurityTokenHandler();

            var nomeUsuario = $"{usuario.Nome} {usuario.Sobrenome}";

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, usuario.Email),
                    new Claim("usuario_id", usuario.Id.ToString()),
                    new Claim("nome", nomeUsuario),
                    new Claim(JwtRegisteredClaimNames.Sub, usuario.Id.ToString()),
                    new Claim(JwtRegisteredClaimNames.Email, usuario.Email),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(DateTime.UtcNow).ToString(), ClaimValueTypes.Integer64),
                }),
                Issuer = Emissor,
                Expires = expiraEm,
                NotBefore = NotBefore,
                IssuedAt = IssuedAt,
                SigningCredentials = new SigningCredentials(SecurityKey(), SecurityAlgorithms.HmacSha256Signature)
            };

            var encodedJwt = tokenHandler.WriteToken(tokenHandler.CreateToken(tokenDescriptor));

            return new TokenDto
            {
                Token = encodedJwt,
                ExpiraEm = expiraEm,
                Usuario = new UsuarioTokenDto
                {
                    Id = usuario.Id.ToString(),
                    Nome = nomeUsuario,
                    Email = usuario.Email,
                }
            };
        }

        public SymmetricSecurityKey SecurityKey() => new SymmetricSecurityKey(Encoding.ASCII.GetBytes(ClientSecret));

        #endregion Public Methods

        #region Private Methods

        private static long ToUnixEpochDate(DateTime date)
                                                => (long)Math.Round((date.ToUniversalTime() - new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero)).TotalSeconds);

        #endregion Private Methods
    }
}