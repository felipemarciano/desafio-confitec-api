﻿using DesafioConfitec.API.Security;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace DesafioConfitec.API.Extensions
{
    public static class JwtConfigurationExtension
    {
        #region Public Methods

        public static void AddJwtBearerIdentityServerConfiguration(this AuthenticationBuilder options, IConfiguration configuration)
        {
            var tokenConfiguration = new TokenConfiguration(configuration);

            options.AddIdentityServerAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme,
                jwtOptions =>
                {
                    jwtOptions.RequireHttpsMetadata = false;
                    jwtOptions.SaveToken = true;
                    jwtOptions.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = tokenConfiguration.SecurityKey(),
                        ValidateIssuer = true,
                        ValidateAudience = false,
                        ValidateLifetime = true,
                        ValidIssuer = tokenConfiguration.Emissor
                    };
                },
                referenceOptions =>
                {
                    referenceOptions.Authority = tokenConfiguration.Authority;
                    referenceOptions.ClientId = tokenConfiguration.ClientId;
                    referenceOptions.ClientSecret = tokenConfiguration.ClientSecret;
                });
        }

        #endregion Public Methods
    }
}