﻿using DesafioConfitec.API.Security;
using DesafioConfitec.Data.UoW;
using DesafioConfitec.Dominio.Dto;
using DesafioConfitec.Dominio.Interfaces.Services;
using DesafioConfitec.Dominio.Notify;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace DesafioConfitec.API.Controllers
{
    [Route("api/v1/account")]
    public class AccountController : BaseController
    {
        #region Private Fields

        private readonly TokenConfiguration _tokenConfiguration;
        private readonly IUsuarioService _usuarioService;

        #endregion Private Fields

        #region Public Constructors

        public AccountController(IUsuarioService usuarioService,
            IConfiguration configuration,
            IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _usuarioService = usuarioService;
            _tokenConfiguration = new TokenConfiguration(configuration);
        }

        #endregion Public Constructors

        #region Public Methods

        // POST api/account
        /// <summary>
        /// Verificar a autenticidade dos dados de login de usuários.
        /// </summary>
        /// <param name="autenticacaoUsuarioDto">Dados de login do usuário.</param>
        /// <remarks>
        /// Exemplo:
        ///
        ///     POST api/account
        ///     {
        ///       "Email": "teste@teste.com.br",
        ///       "Senha": "1234"
        ///     }
        ///
        /// </remarks>
        /// <returns>Retorna os dados de usuário e o token.</returns>
        /// <response code="200">Login realizado.</response>
        /// <response code="400">O login não foi realizado devido algum dado inconsistente.</response>
        /// <response code="404">Não foi encontrado nenhum usuário com as credenciais informadas.</response>
        [AllowAnonymous]
        [HttpPost("login")]
        [ProducesResponseType(typeof(Notification<object>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(Notification<object>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(Notification<object>), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> LoginAsync([FromBody] AutenticacaoUsuarioDto autenticacaoUsuarioDto)
        {
            var notificationUsuario = await _usuarioService.LoginPorEmailAsync(autenticacaoUsuarioDto);
            if (!notificationUsuario.Success)
            {
                return await ResponseAPIAsync(notificationUsuario);
            }

            var usuarioToken = _tokenConfiguration.GerarJwt(notificationUsuario.Data);

            var notification = new Notification<TokenDto>();
            notification.SetOk(usuarioToken);

            return await ResponseAPIAsync(notification);
        }

        #endregion Public Methods
    }
}