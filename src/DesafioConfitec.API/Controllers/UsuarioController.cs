﻿using DesafioConfitec.Data.UoW;
using DesafioConfitec.Dominio.Dto;
using DesafioConfitec.Dominio.Filters;
using DesafioConfitec.Dominio.Interfaces.Services;
using DesafioConfitec.Dominio.Notify;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DesafioConfitec.API.Controllers
{
    [Route("api/v1/usuarios")]
    public class UsuarioController : BaseController
    {
        #region Private Fields

        private readonly IUsuarioService _usuarioService;

        #endregion Private Fields

        #region Public Constructors

        public UsuarioController(IUsuarioService usuarioService, IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _usuarioService = usuarioService;
        }

        #endregion Public Constructors

        #region Public Methods

        // POST api/usuarios
        /// <summary>
        /// Cadastra um usuário.
        /// </summary>
        /// <param name="usuarioDto"></param>
        /// <remarks>
        /// Exemplo:
        ///
        ///     POST api/usuarios
        ///     {
        ///        "Nome": "Luciana",
        ///        "Sobrenome": "Costa de Oliveira",
        ///        "Email": "teste@teste.com.br",
        ///        "DataNascimento": "2020-12-23",
        ///        "Escolaridade": 3,
        ///        "Senha": "1234",
        ///        "ConfirmacaoSenha": "1234"
        ///     }
        ///
        ///    #Escolaridade
        ///     1-Infantil
        ///     2-Fundamental
        ///     3-Médio
        ///     4-Superior
        ///
        /// </remarks>
        /// <returns>O cadastro do usuário criado.</returns>
        /// <response code="201">O cadastro do usuário foi criado.</response>
        /// <response code="401">Você não está logado no sistema para realizar essa operação.</response>
        /// <response code="400">O cadastro do usuário não foi criado devido algum dado inconsistente.</response>
        [AllowAnonymous]
        [HttpPost]
        [ProducesResponseType(typeof(Notification<object>), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(Notification<object>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> AdicionarAsync([FromBody] UsuarioCadastroDto usuarioDto)
        {
            _unitOfWork.BeginTransaction();
            var notification = await _usuarioService.AdicionarAsync(usuarioDto);

            if (!notification.Success)
                return await ResponseAPIAsync(notification);

            await _unitOfWork.CommitAsync();

            return await ResponseAPIAsync(notification);
        }

        // PUT api/usuarios
        /// <summary>
        /// Atualiza o cadastro de usuário.
        /// </summary>
        /// <param name="usuarioDto"></param>
        /// <remarks>
        /// Exemplo:
        ///
        ///     PUT api/usuarios
        ///     {
        ///        "DataNascimento": "2005-12-23",
        ///        "Email": "teste@teste.com.br",
        ///        "Escolaridade": 3,
        ///        "Id": 5,
        ///        "Nome": "Andre",
        ///        "Sobrenome": "da Silva"
        ///     }
        ///
        ///    #Escolaridade
        ///     1-Infantil
        ///     2-Fundamental
        ///     3-Médio
        ///     4-Superior
        ///
        /// </remarks>
        /// <returns>O cadastro do usuário atualizado.</returns>
        /// <response code="200">O cadastro do usuário foi atualizado.</response>
        /// <response code="400">O cadastro do usuário não foi atualizado devido algum dado inconsistente.</response>
        /// <response code="401">Você não está logado no sistema para realizar essa operação.</response>
        /// <response code="404">O cadastro do usuário informado não existe.</response>
        [HttpPut]
        [ProducesResponseType(typeof(Notification<object>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(Notification<object>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(Notification<object>), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> EditarAsync([FromBody] UsuarioAlteracaoDto usuarioDto)
        {
            _unitOfWork.BeginTransaction();
            var notification = await _usuarioService.EditarAsync(usuarioDto);

            if (!notification.Success)
                return await ResponseAPIAsync(notification);

            await _unitOfWork.CommitAsync();

            return await ResponseAPIAsync(notification);
        }

        // DELETE api/usuarios/id
        /// <summary>
        /// Excluir um cadastro de usuário.
        /// </summary>
        /// <param name="id">Id fo usuário a er excluído</param>
        /// <remarks>
        /// Exemplo:
        ///
        ///     DELETE api/usuarios/5
        ///     {
        ///
        ///     }
        ///
        /// </remarks>
        /// <returns>Confirmação da exclusão do cadastro do usuário.</returns>
        /// <response code="200">O cadastro do usuário foi excluído.</response>
        /// <response code="400">O cadastro do usuário não foi excluído devido algum dado inconsistente.</response>
        /// <response code="401">Você não está logado no sistema para realizar essa operação.</response>
        /// <response code="404">O cadastro do usuário informado não existe.</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(Notification<object>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(Notification<object>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(Notification<object>), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> ExcluirAsync(int id)
        {
            _unitOfWork.BeginTransaction();
            var notification = await _usuarioService.ExcluirAsync(id);

            if (!notification.Success)
                return await ResponseAPIAsync(notification);

            return await ResponseAPIAsync(notification);
        }

        // GET api/usuarios/id
        /// <summary>
        /// Obtêm o cadastro de um usuário.
        /// </summary>
        /// <param name="id">Id fo usuário a er excluído</param>
        /// <remarks>
        /// Exemplo:
        ///
        ///     GET api/usuarios/3
        ///     {
        ///
        ///     }
        ///
        /// </remarks>
        /// <returns>Obtêm um cadastro de usuário.</returns>
        /// <response code="200">Os dados do usuário foram retornados.</response>
        /// <response code="401">Você não está logado no sistema para realizar essa operação.</response>
        /// <response code="404">O cadastro do usuário informado não existe.</response>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(Notification<UsuarioDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(Notification<object>), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> ObterPorIdAsync(int id)
        {
            var notification = await _usuarioService.ObterPorIdAsync(id);

            if (!notification.Success)
                return await ResponseAPIAsync(notification);

            return await ResponseAPIAsync(notification);
        }

        // POST api/usuarios/pesquisar
        /// <summary>
        /// Obtêm uma lista de usuários.
        /// </summary>
        /// <param name="filter">Filtro para refinar a pesquisa</param>
        /// <remarks>
        /// Exemplo:
        ///
        ///     POST api/usuarios/pesquisar
        ///     {
        ///         "DataNascimentoInicio": "1970-05-02",
        ///         "DataNascimentoTermino": "1970-08-31",
        ///         "Email": "maria.lourdes@teste.com",
        ///         "Escolaridade": "4",
        ///         "Nome": "Maria",
        ///         "Sobrenome": "de Lourdes"
        ///     }
        ///
        ///     #Escolaridade
        ///     1-Infantil
        ///     2-Fundamental
        ///     3-Médio
        ///     4-Superior
        ///
        /// </remarks>
        /// <returns>Obtêm uma lista de usuários.</returns>
        /// <response code="200">Os lista de usuários foi retornada.</response>
        /// <response code="400">O filtro informado tem algum dado inconsistente e a pesquisa não pôde ser realizada.</response>
        /// <response code="401">Você não está logado no sistema para realizar essa operação.</response>
        /// <response code="404">Não foram encontrados cadastro de usuário para o filtro informado.</response>
        [HttpPost("pesquisar")]
        [ProducesResponseType(typeof(Notification<IList<UsuarioDto>>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(Notification<object>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(Notification<object>), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> PesquisarAsync([FromBody] UsuarioFilter filter)
        {
            var notification = await _usuarioService.PesquisarAsync(filter);

            if (!notification.Success)
                return await ResponseAPIAsync(notification);

            return await ResponseAPIAsync(notification);
        }

        #endregion Public Methods
    }
}