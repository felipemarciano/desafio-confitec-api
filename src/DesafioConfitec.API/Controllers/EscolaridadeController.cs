﻿using DesafioConfitec.Data.UoW;
using DesafioConfitec.Dominio.Dto;
using DesafioConfitec.Dominio.Interfaces.Services;
using DesafioConfitec.Dominio.Notify;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace DesafioConfitec.API.Controllers
{
    [Route("api/v1/escolaridades")]
    public class EscolaridadeController : BaseController
    {
        #region Private Fields

        private readonly IEscolaridadeService _escolaridadeService;

        #endregion Private Fields

        #region Public Constructors

        public EscolaridadeController(
            IEscolaridadeService escolaridadeService,
            IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _escolaridadeService = escolaridadeService;
        }

        #endregion Public Constructors

        #region Public Methods

        // GET api/escolaridades
        /// <summary>
        /// Obtêm uma lista de escolaridade.
        /// </summary>
        /// <remarks>
        /// Exemplo:
        ///
        ///     GET api/escolaridades
        ///     {
        ///
        ///     }
        ///
        /// </remarks>
        /// <returns>Obter uma lista de escolaridades.</returns>
        /// <response code="200">Os dados de escolaridades foram obtidos.</response>
        /// <response code="401">Você não está logado no sistema para realizar essa operação.</response>
        /// <response code="404">Não foram encontradas cadastro de escolaridade.</response>
        [AllowAnonymous]
        [HttpGet()]
        [ProducesResponseType(typeof(Notification<EscolaridadeDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(typeof(Notification<object>), StatusCodes.Status404NotFound)]
        public async Task<IActionResult> ObterescolaridadesAsync()
        {
            var notification = await _escolaridadeService.ObterEscolaridadesAsync();

            if (!notification.Success)
                return await ResponseAPIAsync(notification);

            return await ResponseAPIAsync(notification);
        }

        #endregion Public Methods
    }
}