﻿using DesafioConfitec.Data.UoW;
using DesafioConfitec.Dominio.Notify;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;

namespace DesafioConfitec.API.Controllers
{
    [Produces("application/json")]
    [ApiController]
    [Authorize]
    public class BaseController : Controller
    {
        #region Protected Fields

        protected readonly IUnitOfWork _unitOfWork;

        #endregion Protected Fields

        #region Protected Constructors

        protected BaseController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion Protected Constructors

        #region Internal Methods

        internal async Task<IActionResult> ResponseAPIAsync<T>(Notification<T> notification)
        {
            if (notification == null)
            {
                _unitOfWork.Rollback();
                return BadRequest("Ocorreu um erro não tratado durante sua requisição! Por favor, tente mais tarde.");
            }

            if (notification.Success)
            {
                await _unitOfWork.CommitAsync();
            }
            else
            {
                _unitOfWork.Rollback();
            }

            switch (notification.Status)
            {
                case (int)HttpStatusCode.OK:
                    return Ok(notification);

                case (int)HttpStatusCode.BadRequest:
                    return BadRequest(notification);

                case (int)HttpStatusCode.Created:
                    return StatusCode(HttpStatusCode.Created.GetHashCode(), notification);

                case (int)HttpStatusCode.NotFound:
                    return NotFound(notification);

                default:
                    break;
            }
            return null;
        }

        #endregion Internal Methods
    }
}