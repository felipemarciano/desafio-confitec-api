using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System.IO;

namespace DesafioConfitec.API
{
    public class Program
    {
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseSetting("detailedErrors", "true");
                    webBuilder.UseKestrel();
                    webBuilder.UseIISIntegration();
                    webBuilder.CaptureStartupErrors(true);
                    webBuilder.UseContentRoot(Directory.GetCurrentDirectory());
                    webBuilder.ConfigureAppConfiguration((hostingContext, config) =>
                    {
                        var env = hostingContext.HostingEnvironment;
                        config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true);
                        config.AddEnvironmentVariables();
                    });
                    webBuilder.UseStartup<Startup>();
                });

        #region Public Methods

        //public static IWebHostBuilder CreateHostBuilder(string[] args) =>
        //   WebHost.CreateDefaultBuilder(args)
        //           .UseSetting("detailedErrors", "true")
        //           .UseKestrel()
        //           .UseIISIntegration()
        //           .CaptureStartupErrors(true)
        //           .UseContentRoot(Directory.GetCurrentDirectory())
        //           .ConfigureAppConfiguration((hostingContext, config) =>
        //           {
        //               var env = hostingContext.HostingEnvironment;
        //               config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
        //                       .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true);
        //               config.AddEnvironmentVariables();
        //           })
        //           .UseStartup<Startup>();

        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        #endregion Public Methods
    }
}