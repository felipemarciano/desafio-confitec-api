﻿using DesafioConfitec.API.Extensions;
using DesafioConfitec.API.Middlewares;
using DesafioConfitec.CrossCutting;
using DesafioConfitec.Data.Context;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Logging;
using System.IdentityModel.Tokens.Jwt;

namespace DesafioConfitec.API
{
    public class StartupStaging
    {
        #region Public Constructors

        public StartupStaging(IWebHostEnvironment environment, IConfiguration config)
        {
            HostingEnvironment = environment;
            Configuration = config;
        }

        #endregion Public Constructors

        #region Public Properties

        public IConfiguration Configuration { get; }

        public IWebHostEnvironment HostingEnvironment { get; }

        #endregion Public Properties

        #region Public Methods

        public void Configure(IApplicationBuilder app)
        {
            app.ConfigureExceptionHandler();

            app.UseCors(x =>
            {
                x.AllowAnyHeader();
                x.AllowAnyMethod();
                x.AllowAnyOrigin();
                x.AllowCredentials();
                x.WithOrigins("http://localhost:4200");
            });

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers()
            .AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.PropertyNamingPolicy = null;
            });

            services.AddAuthorization();

            //JWT
            JwtSecurityTokenHandler.DefaultMapInboundClaims = false;
            if (HostingEnvironment.IsDevelopment())
                IdentityModelEventSource.ShowPII = true;

            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                    .AddJwtBearerIdentityServerConfiguration(Configuration);

            services.AddCors();

            services.AddAutoMapper(typeof(StartupStaging));

            services.AddLogging(builder =>
            {
                builder.AddConfiguration(Configuration.GetSection("Logging"))
                    .ClearProviders()
                    .AddConsole()
                    .AddDebug()
                    .AddEventLog()
                    .AddEventSourceLogger();
            });

            var conn = Configuration.GetConnectionString("AppConnection");
            services.AddDbContext<ConfitecContext>(options => options.UseSqlServer(conn));

            services.RegisterDomainServices();
            services.RegisterRepositoriesServices();
            services.RegisterOtherServices();
        }

        #endregion Public Methods
    }
}