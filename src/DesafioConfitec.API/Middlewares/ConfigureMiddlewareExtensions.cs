﻿using Microsoft.AspNetCore.Builder;

namespace DesafioConfitec.API.Middlewares
{
    public static class ConfigureMiddlewareExtensions
    {
        #region Public Methods

        public static void ConfigureExceptionHandler(this IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionMiddleware>();
        }

        #endregion Public Methods
    }
}