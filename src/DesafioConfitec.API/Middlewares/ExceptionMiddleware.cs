﻿using DesafioConfitec.Dominio.Notify;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace DesafioConfitec.API.Middlewares
{
    public class ExceptionMiddleware
    {
        #region Private Fields

        private readonly RequestDelegate _next;

        #endregion Private Fields

        #region Public Constructors

        public ExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        #endregion Public Constructors

        #region Public Methods

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(httpContext, ex);
            }
        }

        #endregion Public Methods

        #region Private Methods

        private static async Task  HandleExceptionAsync(HttpContext httpContext, Exception exception)
        {
            httpContext.Response.ContentType = "application/json; charset=utf-8";
            httpContext.Response.StatusCode = HttpStatusCode.InternalServerError.GetHashCode();

            var notification = new Notification<object>();
            notification.AddError("", "Houve um erro durante o processamento da sua requisição!");
            //Aqui podemos persistir a exception em um banco elastic ou no Elmah. Mensageria com Rabbit é uma possibilidade

            await httpContext.Response.WriteAsync(JsonConvert.SerializeObject(notification));
        }

        #endregion Private Methods
    }
}