﻿using AutoMapper;
using DesafioConfitec.API.Extensions;
using DesafioConfitec.API.Middlewares;
using DesafioConfitec.API.Security;
using DesafioConfitec.CrossCutting;
using DesafioConfitec.Data.Context;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Reflection;

namespace DesafioConfitec.API
{
    public class Startup
    {
        #region Public Constructors

        public Startup(IWebHostEnvironment environment, IConfiguration config)
        {
            HostingEnvironment = environment;
            Configuration = config;
        }

        #endregion Public Constructors

        #region Public Properties

        public IConfiguration Configuration { get; }

        public IWebHostEnvironment HostingEnvironment { get; }

        #endregion Public Properties

        #region Public Methods

        public void Configure(IApplicationBuilder app)
        {
            app.ConfigureExceptionHandler();

            app.UseCors(x =>
            {
                x.AllowAnyHeader();
                x.AllowAnyMethod();
                x.AllowAnyOrigin();
                x.AllowCredentials();
                x.WithOrigins("http://localhost:4200");
            });

            var option = new RewriteOptions();
            option.AddRedirect("^$", "swagger");
            app.UseRewriter(option);

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Confitec API V1");
            });
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers()
            .AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.PropertyNamingPolicy = null;
            });

            services.AddAuthorization();

            //JWT
            JwtSecurityTokenHandler.DefaultMapInboundClaims = false;
            if (HostingEnvironment.IsDevelopment())
                IdentityModelEventSource.ShowPII = true;

            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                    .AddJwtBearerIdentityServerConfiguration(Configuration);

            services.AddCors();

            services.AddAutoMapper(typeof(Startup));
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                    new OpenApiInfo
                    {
                        Version = "v1",
                        Title = "Confitec API",
                        Description = "API REST integração com cadastro de usuário.",
                        TermsOfService = new Uri("https://teste.com.br/terms_a_fazer"),
                        Contact = new OpenApiContact
                        {
                            Email = "felipe.santos@confitec.com.br",
                            Name = "Felipe Santos Marciano",
                        },
                        License = new OpenApiLicense
                        {
                            Name = "À fazer",
                            Url = new Uri("https://example.com/license"),
                        }
                    });

                c.IncludeXmlComments(GetXmlCommentsPath());
            });

            services.AddLogging(builder =>
            {
                builder.AddConfiguration(Configuration.GetSection("Logging"))
                    .ClearProviders()
                    .AddConsole()
                    .AddDebug()
                    .AddEventLog()
                    .AddEventSourceLogger();
            });

            var conn = Configuration.GetConnectionString("AppConnection");
            services.AddDbContext<ConfitecContext>(options => options.UseSqlServer(conn));

            services.RegisterDomainServices();
            services.RegisterRepositoriesServices();
            services.RegisterOtherServices();
        }

        #endregion Public Methods

        #region Private Methods

        private string GetXmlCommentsPath()
        {
            var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
            return xmlPath;
        }

        #endregion Private Methods
    }
}