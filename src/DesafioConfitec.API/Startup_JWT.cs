﻿using AutoMapper;
using DesafioConfitec.API.Middlewares;
using DesafioConfitec.API.Security;
using DesafioConfitec.CrossCutting;
using DesafioConfitec.Data.Context;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using System.Reflection;

namespace DesafioConfitec.API
{
    public class Startup_JWT
    {
        #region Public Constructors

        public Startup_JWT(IWebHostEnvironment environment, IConfiguration config)
        {
            HostingEnvironment = environment;
            Configuration = config;
        }

        #endregion Public Constructors

        #region Public Properties

        public IConfiguration Configuration { get; }

        public IWebHostEnvironment HostingEnvironment { get; }

        #endregion Public Properties

        #region Public Methods

        public void Configure(IApplicationBuilder app)
        {
            app.ConfigureExceptionHandler();

            app.UseCors(x =>
            {
                x.AllowAnyHeader();
                x.AllowAnyMethod();
                x.AllowAnyOrigin();
                x.AllowCredentials();
                x.WithOrigins("http://localhost:4200");
            });

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Confitec API V1");
            });

            var option = new RewriteOptions();
            option.AddRedirect("^$", "swagger");
            app.UseRewriter(option);

            app.UseSwagger();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        public void ConfigureServices(IServiceCollection services)
        {
            

            services.AddControllers()
            .AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.PropertyNamingPolicy = null;
            });

            var tokenConfiguration = new TokenConfiguration(Configuration);

            services.AddAuthorization();

            //JWT
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x =>
            {
                //ValidAudiences e ValidIssuer são listas, deixar no radar para implementar mais de um
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = tokenConfiguration.SecurityKey(),
                    ValidateIssuer = true,
                    ValidateAudience = false,
                    ValidateLifetime = true,
                    ValidIssuer = tokenConfiguration.Emissor
                };
            });

            services.AddCors();

            services.AddAutoMapper(typeof(Startup_JWT));
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                    new OpenApiInfo
                    {
                        Version = "v1",
                        Title = "Confitec API",
                        Description = "API REST integração com cadastro de usuário.",
                        TermsOfService = new Uri("https://teste.com.br/terms_a_fazer"),
                        Contact = new OpenApiContact
                        {
                            Email = "felipe.santos@confitec.com.br",
                            Name = "Felipe Santos Marciano",
                        },
                        License = new OpenApiLicense
                        {
                            Name = "À fazer",
                            Url = new Uri("https://example.com/license"),
                        }
                    });

                c.IncludeXmlComments(GetXmlCommentsPath());
            });

            services.AddLogging(builder =>
            {
                builder.AddConfiguration(Configuration.GetSection("Logging"))
                    .ClearProviders()
                    .AddConsole()
                    .AddDebug()
                    .AddEventLog()
                    .AddEventSourceLogger();
            });

            var conn = Configuration.GetConnectionString("AppConnection");
            services.AddDbContext<ConfitecContext>(options => options.UseSqlServer(conn));
            services.RegisterDomainServices();
            services.RegisterRepositoriesServices();
            services.RegisterOtherServices();
        }

        #endregion Public Methods

        #region Private Methods

        private string GetXmlCommentsPath()
        {
            var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
            return xmlPath;
        }

        #endregion Private Methods
    }
}