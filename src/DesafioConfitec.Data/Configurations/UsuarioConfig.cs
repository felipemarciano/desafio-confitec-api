﻿using DesafioConfitec.Dominio.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DesafioConfitec.Data.Configurations
{
    internal class UsuarioConfig : IEntityTypeConfiguration<Usuario>
    {
        #region Public Methods

        public virtual void Configure(EntityTypeBuilder<Usuario> builder)
        {
            builder.ToTable("Usuarios");

            builder.HasKey(x => x.Id);

            builder.Property(e => e.Id)
                   .ValueGeneratedOnAdd()
                   .IsRequired();

            builder.Property(e => e.DataNascimento).HasColumnType("date");

            builder.Property(e => e.Email)
                .IsRequired()
                .HasMaxLength(100)
                .IsUnicode(false);

            builder.Property(e => e.Nome)
                .IsRequired()
                .HasMaxLength(60)
                .IsUnicode(false);

            builder.Property(e => e.Sobrenome)
                .IsRequired()
                .HasMaxLength(60)
                .IsUnicode(false);

            builder.Property(x => x.Escolaridade)
                .HasColumnType("INT");

            builder.HasOne(d => d.EscolaridadeNavigation)
                .WithMany(p => p.Usuarios)
                .HasForeignKey(d => d.Escolaridade)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Usuarios_Escolaridade");
        }

        #endregion Public Methods
    }
}