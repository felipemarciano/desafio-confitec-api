﻿using DesafioConfitec.Dominio.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DesafioConfitec.Data.Configurations
{
    internal class EscolaridadeConfig : IEntityTypeConfiguration<Escolaridade>
    {
        #region Public Methods

        public virtual void Configure(EntityTypeBuilder<Escolaridade> builder)
        {
            builder.ToTable("Escolaridades");

            builder.HasKey(x => x.Id);

            builder.Property(e => e.Id)
                   .HasColumnType("INT")
                   .ValueGeneratedOnAdd()
                   .IsRequired();

            builder.Property(e => e.Descricao)
                   .IsRequired()
                   .HasMaxLength(100)
                   .IsUnicode(false);
        }

        #endregion Public Methods
    }
}