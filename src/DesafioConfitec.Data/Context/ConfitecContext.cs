﻿using DesafioConfitec.Data.Configurations;
using DesafioConfitec.Dominio.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace DesafioConfitec.Data.Context
{
    public class ConfitecContext : DbContext
    {
        #region Public Constructors

        public ConfitecContext(DbContextOptions<ConfitecContext> options)
            : base(options)
        {
        }

        #endregion Public Constructors

        #region Public Properties

        public virtual DbSet<Escolaridade> Escolaridades { get; set; }

        public virtual DbSet<Usuario> Usuarios { get; set; }

        #endregion Public Properties

        #region Protected Methods

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging();
            optionsBuilder.EnableDetailedErrors();
            optionsBuilder.UseLoggerFactory(LoggerFactory.Create(builder => builder.AddConsole()));

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UsuarioConfig());
            modelBuilder.ApplyConfiguration(new EscolaridadeConfig());
            base.OnModelCreating(modelBuilder);
        }

        #endregion Protected Methods
    }
}