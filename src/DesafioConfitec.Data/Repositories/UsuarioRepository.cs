﻿using Dapper;
using DesafioConfitec.Data.Extensions;
using DesafioConfitec.Data.UoW;
using DesafioConfitec.Dominio.Dto;
using DesafioConfitec.Dominio.Entities;
using DesafioConfitec.Dominio.Extensions;
using DesafioConfitec.Dominio.Filters;
using DesafioConfitec.Dominio.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesafioConfitec.Data.Repositories
{
    public class UsuarioRepository : RepositoryBase<Usuario>, IUsuarioRepository
    {
        #region Private Fields

        private bool _disposed;

        #endregion Private Fields

        #region Public Constructors

        public UsuarioRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        #endregion Public Constructors

        #region Public Methods

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public async Task<Usuario> ObterPorEmailAsync(string email)
        {
            var query = @"SELECT
                                Id,
                                Nome,
                                Sobrenome,
                                Email,
                                DataNascimento,
                                Escolaridade,
                                Senha
                          FROM Usuarios
                          WHERE Email = @email";

            return await _unitOfWork.Context.Database.GetDbConnection().QuerySingleOrDefaultAsync<Usuario>(query, new { email }).ConfigureAwait(false);
        }

        public async Task<Usuario> ObterPorIdAsync(int id)
        {
            var query = @"SELECT
                                Id,
                                Nome,
                                Sobrenome,
                                Email,
                                DataNascimento,
                                Escolaridade
                          FROM Usuarios
                          WHERE Id = @id";

            return await _unitOfWork.Context.Database.GetDbConnection().NewConnection().QuerySingleOrDefaultAsync<Usuario>(query, new { id }).ConfigureAwait(false);
        }

        //public async Task<Usuario> ObterPorIdAsync(int id)
        //{
        //    var query = await _unitOfWork.Context.Usuarios.AsNoTracking().SingleOrDefaultAsync(x => x.Id == id);

        //    return query;
        //}

        public async Task<Usuario> ObterPorIdComSenhaAsync(int id)
        {
            var query = @"SELECT
                                Id,
                                Nome,
                                Sobrenome,
                                Email,
                                DataNascimento,
                                Escolaridade,
                                Senha
                          FROM Usuarios
                          WHERE Id = @id";

            return await _unitOfWork.Context.Database.GetDbConnection().NewConnection().QuerySingleOrDefaultAsync<Usuario>(query, new { id }).ConfigureAwait(false);
        }

        public async Task<IEnumerable<UsuarioDto>> PesquisarAsync(UsuarioFilter filter)
        {
            var query = new StringBuilder(
                          @"SELECT
                                u.Id,
                                u.Nome,
                                u.Sobrenome,
                                u.Email,
                                u.DataNascimento,
                                u.Escolaridade,
                                e.Descricao AS EscolaridadeDescricao
                          FROM Usuarios u
                                INNER JOIN Escolaridades e ON u.Escolaridade = e.Id
                          WHERE 1=1 ");

            var periodoNascimento = filter.DataNascimentoInicio.ValidarPeriodo(filter.DataNascimentoTermino);
            if (periodoNascimento.Result.IsValid)
                query.Append("AND DATE(u.DataNascimento) BETWEEN DATE(@DataNascimentoInicio) AND DATE(@DataNascimentoTermino) ");

            if (filter.Email.EmailObrigatorioValido())
                query.Append("AND u.Email = @Email ");

            if (!string.IsNullOrWhiteSpace(filter.Nome))
                query.Append("AND u.Nome = @Nome ");

            if (!string.IsNullOrWhiteSpace(filter.Sobrenome))
                query.Append("AND u.Sobrenome = @Sobrenome ");

            if (filter.Escolaridade.IsValid())
                query.Append("AND u.Escolaridade = @Escolaridade ");

            return await _unitOfWork.Context.Database.GetDbConnection().QueryAsync<UsuarioDto>(query.ToString(), filter).ConfigureAwait(false);
        }

        public async Task<bool> VerificarDuplicidadeAsync(int id, string nome, string sobrenome, string email)
        {
            var query = @"SELECT
                                Id,
                                Nome,
                                Sobrenome,
                                Email
                          FROM Usuarios
                          WHERE Id <> @id AND
                                (Nome = @nome OR
                                 Sobrenome = @sobrenome OR
                                 Email = @email
                                )";

            var result = await _unitOfWork.Context.Database.GetDbConnection().NewConnection().QueryAsync<Usuario>(query, new { id, nome, sobrenome, email }).ConfigureAwait(false);
            return result.Any();
        }

        #endregion Public Methods

        #region Private Methods

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _unitOfWork.Context?.Dispose();
                }
                _disposed = true;
            }
        }

        #endregion Private Methods
    }
}