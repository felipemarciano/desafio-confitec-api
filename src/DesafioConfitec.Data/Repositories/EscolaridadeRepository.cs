﻿using Dapper;
using DesafioConfitec.Data.UoW;
using DesafioConfitec.Dominio.Dto;
using DesafioConfitec.Dominio.Entities;
using DesafioConfitec.Dominio.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DesafioConfitec.Data.Repositories
{
    public class EscolaridadeRepository : RepositoryBase<Escolaridade>, IEscolaridadeRepository
    {
        #region Private Fields

        private bool _disposed;

        #endregion Private Fields

        #region Public Constructors

        public EscolaridadeRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        #endregion Public Constructors

        #region Public Methods

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public async Task<IEnumerable<EscolaridadeDto>> ObterEscolaridadesAsync()
        {
            var query = @"SELECT Id, Descricao FROM Escolaridades ORDER BY Id";

            return await _unitOfWork.Context.Database.GetDbConnection().QueryAsync<EscolaridadeDto>(query).ConfigureAwait(false);
        }

        #endregion Public Methods

        #region Private Methods

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _unitOfWork.Context?.Dispose();
                }
                _disposed = true;
            }
        }

        #endregion Private Methods
    }
}