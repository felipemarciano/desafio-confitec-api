﻿using DesafioConfitec.Data.UoW;
using DesafioConfitec.Dominio.Interfaces.Repositories;

namespace DesafioConfitec.Data.Repositories
{
    public class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        #region Private Fields

        protected readonly IUnitOfWork _unitOfWork;

        #endregion Private Fields

        #region Protected Constructors

        protected RepositoryBase(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #endregion Protected Constructors

        #region Public Methods

        public void Adicionar(T objeto)
        {
            _unitOfWork.Context.Attach(objeto).State = Microsoft.EntityFrameworkCore.EntityState.Added;
            _unitOfWork.Context.Set<T>().Add(objeto);
        }

        public void Editar(T objeto)
        {
            _unitOfWork.Context.Attach(objeto).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            _unitOfWork.Context.Set<T>().Update(objeto);
        }

        public void Excluir(T objeto)
        {
            _unitOfWork.Context.Attach(objeto).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
            _unitOfWork.Context.Set<T>().Remove(objeto);
        }

        #endregion Public Methods
    }
}