﻿using DesafioConfitec.Data.Context;
using System.Threading.Tasks;

namespace DesafioConfitec.Data.UoW
{
    public interface IUnitOfWork
    {
        #region Public Properties

        ConfitecContext Context { get; }

        #endregion Public Properties

        #region Public Methods

        void BeginTransaction();

        Task CommitAsync();

        void Rollback();

        #endregion Public Methods
    }
}