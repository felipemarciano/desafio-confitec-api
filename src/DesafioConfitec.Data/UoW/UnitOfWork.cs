﻿using DesafioConfitec.Data.Context;
using System.Threading.Tasks;

namespace DesafioConfitec.Data.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        #region Public Constructors

        public UnitOfWork(ConfitecContext context)
        {
            Context = context;
        }

        #endregion Public Constructors

        #region Public Properties

        public ConfitecContext Context { get; }

        #endregion Public Properties

        #region Public Methods

        public void BeginTransaction()
        {
            if (Context.Database.CurrentTransaction == null)
                Context.Database.BeginTransaction();
        }

        public async Task CommitAsync()
        {
            if (Context.Database.CurrentTransaction == null)
                return;

            await Context.SaveChangesAsync();
            Context.Database.CommitTransaction();
        }

        public void Rollback()
        {
            if (Context.Database.CurrentTransaction == null)
                return;

            Context.Database.RollbackTransaction();
        }

        #endregion Public Methods
    }
}