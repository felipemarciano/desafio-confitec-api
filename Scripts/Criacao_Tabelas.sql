﻿CREATE TABLE [dbo].[Escolaridades] (
    [Id]        INT           IDENTITY (1, 1) NOT NULL,
    [Descricao] VARCHAR (100) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

CREATE TABLE [dbo].[Usuarios] (
    [Id]             INT           IDENTITY (1, 1) NOT NULL,
    [Nome]           VARCHAR (60)  NOT NULL,
    [Sobrenome]      VARCHAR (60)  NOT NULL,
    [Email]          VARCHAR (100) NOT NULL,
    [DataNascimento] DATE          NOT NULL,
    [Escolaridade]   INT           NOT NULL,
    [Senha]          VARCHAR (200) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

ALTER TABLE [Usuarios] ADD CONSTRAINT FK_Usuarios_Escolaridade FOREIGN KEY (Escolaridade)
    REFERENCES Escolaridades(Id);

INSERT INTO Escolaridades (Descricao) VALUES('Infantil');
INSERT INTO Escolaridades (Descricao) VALUES('Fundamental');
INSERT INTO Escolaridades (Descricao) VALUES('Médio');
INSERT INTO Escolaridades (Descricao) VALUES('Superior');